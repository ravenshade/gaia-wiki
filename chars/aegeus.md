<!-- TITLE: General Aegeus -->
<!-- SUBTITLE: Former King of Artem, General of Morean Military -->

# Aegeus
* Wood Elf -- frequently disguises himself 
* Age 205
* Arcane Trickster Rogue
* Favors set of elven knives
* Married to [Alleria](/chars/alleria)
* Son: [Theseus](/chars/theseus) (rumor that it was with Artemis)
* Accent: Deep southern Morean

## Disguise
* Disguises as a human general
* Hides accent as well

# Stats
* Level 11 Arcane Trickster Rogue

# History
## Leadership
* Has ruled the city of Artem since its inception
* Artemis herself gave him the rare honor, rumor has it that they made love
* Ruled city with a fair and free policy

## Morean Takeover
* City initially held a neutral stance during the war
* The militarized city of Argos invaded during the start of the war
* Aegeus agreed to surrender the city assuming no harm would be brought to the city and that he could rule
* His wife and chief commander, [Alleria](/chars/alleria) disagreed so strongly that she left and started the Fletcher Rebellion.

# Plans
* Wishes to win great fame as a general of the Despotate and gain fame for Artem
* Currently in charge of the siege of Knossos