<!-- TITLE: The Bandu Troupe -->
<!-- SUBTITLE: Exotic music, dancing, and theatre from the far lands of Arcadia -->

# The Bandu Troupe
 * Band of traveling performers from the lands of Arcadia

# Main Members
#### Celeste
High Elven singer

#### Captain Nebeway
Human manager of the Troupe
Owns an Inn called The White Dragon where the performances began.

#### Roper Klacks
Old human man, dressed in robes and insignias.
Claims to be an evil alchemist in most plays. Usually attempts to capture the element of wind through calculations.

Puts on puppet-shows about various events.
Runs a group called Wicked Witches & Warlocks Anonymous.

####  Crow
A crazy sarcastic talking crow.

#### Stuart
(A stuart duplicate) musician that joined the group for the free food and housing.

#### Ben-Bandu
Little mole-like creature "mascot" 
He is a rare "Bandu" creature