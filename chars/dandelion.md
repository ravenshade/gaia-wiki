<!-- TITLE: Dandelion -->
<!-- SUBTITLE: White-haired Hafling, daughter of Jeremy the Druid  -->

# Dandelion
* White-haired halfling
* Insists on being called Poppy
* Currently lives in the village of Amore
* Accent: Southern Morean, but tries to make it Athenian
* Lv 1 Druid of the Moon

# Lovers
* Courted by Lucidius 
* Courted by Theseus