<!-- TITLE: Smuggler Vasso -->
<!-- SUBTITLE: Black Market Vasso Contracts Assassinations -->

# Vasso
* Voice: Grim, Bandit

# Wares
* Sells KingKiller Posion: cost is to kill a king

# Location
* Backwoods at [Artem](/places/morea/artem)
# Contracts
* Contract with [Party](/party) - Emmett - to kill King Minos