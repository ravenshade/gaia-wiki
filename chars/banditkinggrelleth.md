<!-- TITLE: Bandit King Grelleth -->
<!-- SUBTITLE: The Leader of the Bandit Camp -->

# Grelleth
* Voice: Angry, gruff, tired of all this shit
* Massively overweight

# Relations
* Maintains "good" relationship with the town of [Gelatin](/places/gelatin)
* Regularly paid off by the King
* Owns the [Bandit Camp](/places/gelatin/bandit_camp)