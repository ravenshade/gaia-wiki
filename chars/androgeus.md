<!-- TITLE: Androgeus (dead) -->
<!-- SUBTITLE: Son of King Minos -->

# Androgeus
* Half-elf, half Dwarf
* Son of [Minos](/chars/king-minos) and Pasiphae
* Challenged in Bull Run by [Theseus](/chars/theseus)
* Killed in Bull Run by Theseus

# Traits
* Hates Artem, especially Theseus (always been compared to him)
* Prays to Zeus, supposedly he is a descendent of
* Agile, witty, arrogant
* low self esteem masquerated as hubris

# Plots
* General Aegeus planned to murder him during the challenge.
* His son executed the plan perfectly.