<!-- TITLE: Archimedes -->
<!-- SUBTITLE: Legendary Engineer, Physicist, and Mathematician of Athens -->

# Archimedes
* [See Wikipedia Doc](https://en.wikipedia.org/wiki/Archimedes)

# Relations
* Siege General of the Despotate
# Land/Location
* Currently living in [Athens](/places/morea/athens)

# Goals