<!-- TITLE: Theseus (dead) -->
<!-- SUBTITLE: Former Heir to Artem -->

# Theseus
* [doc](https://en.wikipedia.org/wiki/Theseus)
* Race: Wood Elf
* Age: 22
* Secretly a demigod of Artemis (powers similar to Hercules)
* Parents are [Aegeus](/chars/aegeus), Alleria
* Wishes to prove himself and find love (may fall in love with Dandelion or Ariadne)
* Wears sandals all the time, wields a sword
* Accent: Southern Morean
* Killed by Quadarius.

# Traits
* Clever, brave, strong, good with disguise.
* Deceptive & Forgetful
* Boastful

# Land/Location
* Lives in Artem
# Relation
* Previous Prince of [Artem](/places/morea/artem)