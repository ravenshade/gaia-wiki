<!-- TITLE: Lydia Marcheschi -->
<!-- SUBTITLE: Daughter of the Marcheschi Vineyards -->

# Lydia Marcheschi
<img src="/uploads/chars/lydia.jpg" width="320" />

* High Elven Woman, age 39
* Sorceress
* Future countess

# Family
 * Mother: [Countess Astrid Marcheschi](/chars/astrid-marcheschi)

# History
 * Spent considerable amount of time at The Heady Topper
 * Agreed with Patriots to impersonate Ariadne as the Queen of Knossos