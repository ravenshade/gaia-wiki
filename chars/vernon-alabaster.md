<!-- TITLE: Vernon Alabaster -->
<!-- SUBTITLE: Paladin, Leader of the War Dogs, Soldier of Morea, survivor of Daedalus Labyrinth -->

# Vernon Alabaster
 * Human, 58
 * Voice: Gruff, older voice.
 * Personality: Direct, in-charge, militant, soft-hearted. Lawful-Good. Values Loyalty most.
 * Notable Features: Large basilisk injury on leg, fear of basilisks.
 * Background: Knight of the Order
 * Oath of the Crown (to Corinth King).
 * Commander of the War Dogs contingent (disappeared ~15 years ago)

# Stats
 * Lv 7 Paladin
 * STR 16 DEX 10 CON 16 INT 10 WIS 10 CHA 14
 * 66 HP, AC 18

# Items
 * Morea Shield
 * Morea Plate Armor
 * Dog Tags w/ picture of wife & miniature of Cerberus (Holly Alabaster) (Holy Symbol)
 * Howitzer's Muzzle, magic, +1 Hammer (1hand, d8, versatile).
 * Lion's Tooth Door Key

# Spells
 * [standard paladin](http://5etools.ravenshade.com/spells.html#blankhash,flstclass:paladin=1)
 * compelled duel
 * command
 * aura of vitality (soldiers nearby +2d6)
 * zone of truth
 * warding bond

# History
 * During the Labyrinth's escape to Mechanus, Vernon was able to escape when heroes did. He and a contingent of his War Dogs escaped outside Knossos and are now performing raids on the city and Delian soldiers nearby. 
 * Party found Vernon, rescued him from his wounds (but it was their fault). 
 * Vernon fetched City Stone for players, but was injured when using the Corrupted Basilisk Key the party gave him.
 * Vernon introduced party to War Dog Old man Brash who had previously gotten all stones & keys
 * Vernon told Party that Guardians are coming and needed their assistance to defend against, then he would help them get out of the maze
 * Party used white flag to insist war dogs listen, Vernon allowed (b/c he recognized a morean insignia on gear (that the party STOLE))
 * Party meets Vernon while skulking through the deep forest. Ambushed by War Dogs, but party fought them off.
# Backstory
 * The Lost Ones regularly meet with War Dogs in exchange for protection and food. 
 * The Lion's Tooth Door remains sealed stopping anyone from solving the puzzle. The puzzle room has become another base of operations.
 * Vernon formed the War Dogs Base in the Forest of Daedalus' Labyrinth and regularly logs the forest for materials. The War Dogs raid all dispatches of Daedalus' minions and engineers to prevent them from fixing "The Dangerous Delight" puzzle.
 * Part of the 17th battalion of Morean Army, nickname "War Dogs." Was sent to Knossos before war in a peaceful operation to request that Knossos join the Despotate of Morea. [King Minos](/chars/king-minos) threw him and all his soldiers into [Daedalus' Labyrinth](/places/morea/daedalus-labyrinth). 
 * Joined Despotate of Morea upon forming. Grew up in Corinth. Married young to Holly Alabaster. Had several kids all living in Corinth.