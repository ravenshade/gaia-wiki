<!-- TITLE: Brandy -->
<!-- SUBTITLE: A fine girl from Gelatin -->

# Brandy
* Hot witch from the town of [Gelatin](/places/gelatin)
* Controls a lot of farmhands by looking at them cute
* Cures a lot of simple ailments the town suffers from

# Relations
* Led the Peasant Revolt in Gelatin