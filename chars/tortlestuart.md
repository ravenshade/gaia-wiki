<!-- TITLE: Stuart the Tortle -->
<!-- SUBTITLE: An Atlantis version of Stuart -->

# Stuart
* Was alive during the fall of [Atlantis](/places/atlantis)
* Was transformed into a Tortle to survive.

# Relations
* Member of [The Stuarts](/orgs/stuarts)