<!-- TITLE: Duplicamos -->
<!-- SUBTITLE: A powerful sorceror capable of duplication magic -->

# Duplicamos
* A human sorceror - split into master/apprentice.
* Known as "The Cloner"
* Voice: Dull, quick, calm.
* Sigil is a duplicated curve across a mirror
* Produces portals using duplication magic
* Has the ability to cast powerful illusion magic.
* Accidentally duplicated himself, decided to take on that duplicate as an apprentice.
* Is known for creating [The Stuarts](/orgs/stuarts) accidentally.

# Master
 * Has ambitions of becoming a god
 * Regularly uses the 'Clone' spell to create future copies of himself that are young
# Apprentice
* Also calls himself "Duplicamos"
* Far more naive, joined the Gaia Initiative (see relations) to prove to betray his master, gain power over him.

# Spells
* Duplicate an area that traverses real space but has a start and end portal.
* Other forms of [duplication magic](/magic/duplication)

# Relations
* A member of the [Gaia Initiative](/orgs/gaia_initiative)
* Tentatively allowed to be a part because he owns Turre Antiqui. 

# Land
* Owns [Turre Antiqui](/places/gelatin/turre_antiqui), the Ancient Tower.

