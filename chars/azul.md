<!-- TITLE: Lord Azul the Lich -->
<!-- SUBTITLE: The legendary necromancer from the Straits -->

# Azul
 * Lich (Undead, once Human), Age 166
 * Once was the Great King Sisyphus of Miletus
 * Married to Merope, who is immortal & his Phylactery
 * Working for [Gaia Initiative](/orgs/gaia_initiative) to raise an undead army
 * Worships Persephone due to a contractual obligation
 * Known as being the "craftiest" human
 * Blue eyes, skeletal frame, long blue and purple robes, a withering long beard, floats

# Merope
 *  a [Pleiades](/races/pleiades) - immortal, like a celestial
 *  Perfect skin, radiant blue eyes, twinkles, covered in fine jewelry, long black hair
 *  Occasionally become partially translucent, light acts strange around her
 *  Her heart is a Phylactery for Azul

# The Howling Manor
 * Contains a spring of endless water (gift from River God)
 * All objects are animated and do work for him
 * His wife lives in the manor but occasionally spends time in a Demiplane or Ethereal plane
 * Has an undead army in the works outside
 * Regularly must leave to collect souls for his wife, but does so with an invisibility cloak
 * The manor has multiple entrances - [Straits of Gibraltar](/places/delia/straits-of-gibraltar), [Arcadia](/places/morea/arcadia), ???
 * The manor itself exists inside an expanded Demiplane full of plants (for Persephone, oxygen, food, etc)
 * Unlike Mordenkainen's Magnificent Mansion, this is an actual structure built to be permanent where he can hide.
 * Some rooms go to different areas in the Demiplane where he keeps bodies, gems, treasures which Instant Summons is used to fetch
 * Entrances are only open if Lord Azul has opened the plane for the guests, which keeps it well hidden.

# Rare Posessions
 * Spring of Endless Water
 * Thanatos' Invisibility Cloak of the Deadly Hollows (stolen in one of his trips)
 * Eros' Bow (cursed) - Eros used it to unite Azul and Merope, but Thanatos' curse hit the bow as well. Now is at the bottom of the Miletus Dungeons under the old castle

# Plans / Contracts
## Gaia Initiative - Undead Army
 * Building an army of undead and skeletons. 
 * Requested by Thanatos for Hades

## Pact with Persephone
 * So long as his wife lives, he may stay alive.
 * So long as he lives, he must also produce Reapers for Persephone.

## Catatonic Arcadian Blight, "Melting Pox"
 * Argus recently funded Azul to develop a disease that targets goliaths for defeating the Spartans.
 * Azul created a fine air-based gas that infects the imbebber with the virus. Works on all beings, but especially Goliaths.
 * The virus spreads and begins melting parts of the skin away. Slowly decaying internally until the Goliath dies.
 * The only cure requires a rare venom from Arcadia, thus why the true name is kept secret.
 * Azul discovered such a capability when he visited Arcadia, from the [Cave of Ten Thousand Monsters](/places/morea/arcadia/cave-of-ten-thousand-monsters).

# History
## Defense against Hormes
 * Fought against invaders with the nearby kingdom of Kor.
 * When the earth began to quake, the kingdom was dragged away from the great mountains
 * Miletus continued to send aid, but after it's fall (see Lich), no one knew why or what happened.


## Becoming a Lich
 * Previously ruled over the land of Miletus as King Sisyphus with Queen Merope
 * Saw Zeus stealing a woman named Aegina from her father (for sex)
 * Told her father, a River God, about Zeus' new mistress. He was able to free her.
 * Zeus punished Sisyphus by having Thanatos the God of Death come drag him to Tartarus
 * Sisyphus cheated Zeus, by noticing Charon wasn't present, and trapping him in Tartarus with his own manacles
 * The God of War, Ares, was angered because while Thanatos wasn't present, no one died. His armies were ineffective, it was boring.
 * Ares freed Thanatos, and Thanatos came after Sisyphus
 * Sisyphus, knowing his fate, asked his wife in an act of love to throw his body naked in the middle of Miletus.
 * When he was dragged back to Tartarus, his wife threw his body in the center, thus forcing him to become unburied and dragging him out of Tartarus.
 * Once outside the river, he met with Persephone. He persuaded her that he must get his revenge on his wife for disrespecting him and in exchange will build Reapers for Persephone, which she could gift to Hades.
 * Persephone brought him back to life so long as his wife lives, so that he may kill her for revenge. However, Sisyphus recognized that he could use this pact for a ritual he once learned of, to become a Lich.
 * Back in his mortal body, he performed the ritual of transformation -- his wife became his phylactery because of Persephone's powers. He sacrificed all his royal officers to build the potion of transformation.
 * He drank the potion of transformation, thus becoming Lord Azul. 
 * The entire Kingdom was cursed by Thanatos to death, including him -- but he rose again from his phylactery.
 * He hides from Thanatos and appeases him and the other gods of the underworld with gifts and favors.
 * His wife's star "has faded" but still exists, lurking in the dark shadows of the sky.