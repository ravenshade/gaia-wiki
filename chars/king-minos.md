<!-- TITLE: King Minos -->
<!-- SUBTITLE: King of Knossos and the Great Forges -->

# King Minos
<img src="/uploads/dwarven-king-1.jpg" width="400" />

* Race: Dwarf
* Age: 107 (Dead)
* King of Knossos
* Judge of the Underworld

# Family
* [Ariadne](/gods/ariadne) - Daughter
* [Androgeus](/chars/androgeus) - Son
* Pasiphae - Wife (dead)
# History
* Gifted a bull he was supposed to sacrifice by Poseidon. Poseidon wanted revenge.
* Poseidon cursed his wife to want to fuck bulls.
* Pasiphae copulated with a bull sent from Poseidon. She bore a Minotaur baby.
* Minos came up with a plan to give a gift to Poseidon (for forgiveness, to save his wife) and destroy the Moreans. 
* After destroying part of a Pillar of Hercules to drown the Morean army, he perished under its weight and the water rushing in as foretold by the Fates.