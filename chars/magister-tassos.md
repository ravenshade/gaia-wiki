<!-- TITLE: Magister Tassos -->
<!-- SUBTITLE: Commander of the Morean Tip Athenian Stronghold -->

# Magister Tassos
* Killed Lucidious

# Spells
* fire bolt, light, mage hand, presti, shocking graps
* (1) detect magic, identify, mage armor, magic missile
* (2) detect thoughts, mirror image, misty step
* (3) fly, lightning bolt, fireball
* (4) banishment, fire shield, stoneskin\*
* (5) cone of cold, scrying, wall of force
* (6) globe of invulnerability

# Quarters Loot
* 5668 G 137 S
* Clockwork staff
* Statue of an Obsidian Steed
* Oil of Sharpness
* 2 Potions of Health
* Dust of Disappearance
* Clockwork Amulet
* Manual of Clay Golems
* Flametongue Sword

# Plans
* War Plans: Maintain Artem, Filter troops from Argos into Stronghold. Destroy temple.
* Plans for army near Babylon Crater to move through mountains, meet w/ forces crossing river, attack knossos, and ships from Argos sailing.
* Plans for machine to destroy Temple of Artemis doors
* Plans for weak spots in the Knossos Castle
* (secret) plans to humiliate General Aegeus