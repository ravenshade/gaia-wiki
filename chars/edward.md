<!-- TITLE: Sir Edward the Philosopher -->
<!-- SUBTITLE: A well-known philosopher and debater -->

# Edward
* Born in [Gelatin](/places/gelatin) in 196.
* Possesses the [Last Word](/items/last_word) given to him by [Athena](/gods/athena)