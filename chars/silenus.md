<!-- TITLE: Silenus -->
<!-- SUBTITLE: A Saytr Blood Demon famous for his writ -->

# Silenus
* A [saytr](/mobs/saytr) blood demon
* Known as "The Puppeteer"
* Voice: Deep
* Sigil is a contract with a snake signature

# Spells
* Powerful [blood magic](/magic/blood) that can control others through objects such as dolls or instruments

# Relations
* A member of the [Gaia Initiative](/orgs/gaia_initiative)
* A subject of [Hades](/gods/hades)

# Goals
* Works with [Proci Deceptis](/chars/proci_deceptis) to produce the powerful voodoo dolls
* Wants to teach mortals blood magic