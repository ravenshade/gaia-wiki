<!-- TITLE: Kreios Epimetheus -->
<!-- SUBTITLE: Kreios - John's Character -->

# Kreios
* John's Character
* Wizard + Gladiator

# Goal
* Become recognized by Athena or Ares for his combat skills
# Backstory
* Born in Apol, lived modest lifestyle
* Close to mother
* Father was a distant successful military figure in the Delian army
* Saw a gladiator fight, knew he wanted to be one
* Particularly loves Priscus the gladiator