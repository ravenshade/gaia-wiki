<!-- TITLE: Lucidius (dead) -->
<!-- SUBTITLE: Shy Wood Elf looking for Love -->

# Lucidius
* Worked for the Fletcher Rebellion in hopes of making a difference as a recruiter
* Former resident of Artem, new resident of Amore village.
* In love with [Dandelion](/chars/dandelion)
* Deceased, killed by [Magister Tassos](/chars/magister-tassos) after being tortured for information on the party.