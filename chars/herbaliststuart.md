<!-- TITLE: Stuart the Herbalist -->
<!-- SUBTITLE: Small-town Herbalist near Gelatin -->

# Stuart
* Free-spirit. Likes to travel.
* Every couple years comes back to Gelatin to recouperate.

# Relations
* Member of [The Stuarts](/orgs/stuarts)

# Duplications
Stuart was duplicated by [Duplicamos](/chars/duplicamos) unbeknownst to him. (everyone is Stuart joke)
* [Stuart the Alchemist](/chars/alchemist_stuart)