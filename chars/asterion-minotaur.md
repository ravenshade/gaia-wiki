<!-- TITLE: Asterion the Minotaur -->
<!-- SUBTITLE: The Minotaur of Minos -->

# Asterion
<img src="/uploads/asterion.png" width="320" />

* Born from Pasiphae, sister of [Ariadne](/gods/ariadne)
* Age: ~60 years old (though born 20 years ago)
* Trapped in the middle of a labrynth
* Voice: Tauren (war3)

# Traits
* Brave, strong, well-learned, frequent reader, noble, knowledable.
* Bull-personality randomly perks up. Short tempered. Stubborn.

# Sheet
* Lv ?? Barbarian Minotaur, AC 14 (Plate 18), HP 250, Sp 40ft, Large.
* Str 20 (+5), Dex 7 (-2), Con 18 (+4), Int 17 (+3), Wis 18 (+4), Cha 8 (-1)

## Features
* Hammering Horns: Any melee attack you can shove with reaction. Str saving throw. 
* Goring Rush: Anytime dash is used, a melee attack is allowed.
* Horns: Weapon, str mod.
* Labyrinthine Recall: Can recall any path it has traveled.
* Reckless: Gain advantage on all melee attack rolls, all attacks have advantage on it.
# History
* Born and immediately locked up in the labrynth (~30 years ago)
* Spent the whole time (~60 years) reading the vast library inside the labyrinth
* Rarely visited by Daedalus who continued to provide him educational material, info from the outside world.