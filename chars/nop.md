<!-- TITLE: Nop (dead?) -->
<!-- SUBTITLE: Nop, famous bard of the Heady Topper -->

# Nop
 * Due to a favor of Daedalus, swapped bodies with [Ariadne](/gods/ariadne-nop).
 * Killed himself while swapped. Both consciousnesses are now embedded inside Ariadne.