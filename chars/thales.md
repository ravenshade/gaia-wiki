<!-- TITLE: Thales -->
<!-- SUBTITLE: Archmage of the Despotate -->

# Thales
* High Elf
* Age 149
* Lv. 17 Wizard

# Land/Location
* Lives in [Argos](/places/morea/argos)
# Relations
* Archmage of the Despotate of Morea
* Member of the [Seven Sages of Gaia](/orgs/sevensagesofgaia)