<!-- TITLE: Queen Alleria -->
<!-- SUBTITLE: Former Queen of Artem, leader of Fletcher Rebellion -->

# Alleria
* Wood Elf
* Age 143
* Married to [Aegeus](/chars/aegeus)
* Ran the Fletcher Rebellion

# Stats
* Level 7 Fighter (Bow)
* Wields +1 Elven Bow that ensares targets