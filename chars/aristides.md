<!-- TITLE: General Aristides -->
<!-- SUBTITLE: Aristides the Despotate General -->

# Aristides
* Nicknamed "The Just"
* [Doc](https://en.wikipedia.org/wiki/Aristides)

# Land/Location
* From [Corinth](/places/corinth)
# Relations
* General of the Despotate of Morea