<!-- TITLE: General Pericles -->
<!-- SUBTITLE: The Great Athenian General -->

# Pericles
* Human
* Age: 44
* [Doc](https://en.wikipedia.org/wiki/Pericles)
* Famous General of Athens

# Relations
* General of the Despotate of Morea