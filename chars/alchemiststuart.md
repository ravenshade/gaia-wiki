<!-- TITLE: Stuart the Alchemist -->
<!-- SUBTITLE: A duplicated version of Stuart the Herbalist -->

# Stuart
* Unaware that he is a duplicate
* Likes to travel

# Relations
* Member of [The Stuarts](/orgs/stuarts)
# Alchemy Career
* Focused on the manufacturing of the legendary [Tailor's Potion](/items/tailors_potion)