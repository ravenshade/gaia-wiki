<!-- TITLE: Thea Thunderbrew -->
<!-- SUBTITLE: Daughter of the Thunderbrew Brewers -->

# Theadora "Thea" Thunderbrew II
<img src="/uploads/chars/thea.png" width="320" />

* Dwarven noble, age 21
* Sorceress
* Future Baroness

# Family
 * Father: [Baron Hammerlin Thunderbrew](/chars/hammerlin-thunderbrew)
 * Mother: [Baroness Theadora Thunderbrew](/chars/theadora-thunderbrew)