<!-- TITLE: Daru the tortle Druid -->
<!-- SUBTITLE: A moon druid -->

# Daru

## History
Daru was born in the Morean tip, living most of his life in a farming village. His gifts began to appear when he was young, much to the dismay of his parents. They were approached by some of the priests of Artemis who offered him tutelege on his powers.
After a long bit of training, he wanted to join an enclave of his own to foster the earth. Daru became one of the keepers of the living forest, north of Kor'kash. This forest is full of awakened plants, living together like an animal community.
Unfortunately, he noticed a sort of... unhealthy presence in Kor'kash. He saw a kind of plague that was infecting the earth, and he knew it was his duty to stop it. He left the living forest to investigate the plagued plants, but...
The plague had partially corrupted him. He began to become paranoid, seeing strange things that didn't exist. The water had done significant damage to his mental state.
Using his druidic powers, he attempted to cleanse a section of the swamp, the Daru Thicket. He grew massive thorns that tried to protect against the plague. While he was able to stop it from fully corrupting him, it did leave him in an injured state, fighting off madness.

### Daru defeats the dragon
 * Daru defeated the great dragon Hormes with the Patriots
 * Daru is now healing the magical swamp back into the forest it once was.