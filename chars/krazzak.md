<!-- TITLE: Krazzak -->
<!-- SUBTITLE: Krazzak the Imp Master -->

# Krazzak
* The Imp Master - a Saytr trapped in an orb
* Voice: Genie
* Lawful Neutral
* Hates Hades, wants revenge on him (blames him for Persephone's wrath).
* Warlock (5th lvl spells), Pact of Chain, Patron of Hades (disowned)

# Attack
* 1d20 + 1
* 1d4 poison damage
* Can change shapes

## Spells
* Krazzak cannot sleep, rest, or regain any magic since he has been cut off from Hades.
* Krazzak can accept spells from outside the orb to cast (if he is capable of casting).
* Krazzak can cast spells, but it requires a blood sacrifice (to control magic outside the orb) equal to (spell level / 2) lives.

# History
* Once a powerful warlock through [Persephone](/gods/persephone). She insisted that he use his imps for gardening.
* Owned a several acre estate where his imps produced large amounts of grain.
* In his off time, his imps went around causing mayhem in nearby cities, particularly [Corinth](/places/morea/corinth).
* Persephone discovered a low harvest once due to this additional mayhem and revoked his imp-based powers. 
* [Duplicamos](/chars/duplicamos) offered him replacement power, stuck him inside of an [orb](/items/krazzaks_orb).

# Goals
* Hide from Hades. Start an imp farm again.