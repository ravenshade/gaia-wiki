<!-- TITLE: Fleet Admiral Conon -->
<!-- SUBTITLE: Conon of Athens-->

# Conon
* See [doc](https://en.wikipedia.org/wiki/Conon)
# Relations
* Fleet Admiral of the Despotate of Morea
# Land/Location
* Lived in [Athens](/places/morea/athens)
* Currently travels with the battlefront