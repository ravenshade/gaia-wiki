<!-- TITLE: Leucis Creed -->
<!-- SUBTITLE: A quick summary of Leuciscreed -->

# Leucis

# A Soul Chained
 * Quadarius and Ariadne made a pact with a Reaper, and thus Persephone, Hades, and Thanatos, to bring Leucis' soul back from the Underworld.
 * They provided a soul in exchange, which performed the Ritual of Transformation.
 * Leucis' soul is currently now forced inside his body.
 * He is about halfway through the ritual to become a Lich -- but he has not yet created and drank a Potion of Transformation.
 * Until the ritual is complete -- if his body is completely destroyed, his soul will be shattered. 