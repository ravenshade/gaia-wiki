<!-- TITLE: Daedalus -->
<!-- SUBTITLE: The Great Architect of Knossos -->

# Daedulus
* Human architect
* Employed by [King Minos](/chars/king-minos)
* Created the legendary labryinth

# Traits
* Incredibly intelligent and knowledgable. 
* Andrew Ryan like qualities. Wants to make his own rapture.
* Gifted with time magic through rigorous study. Resents sorcery.

# Plans
## The Escape to Mechanus
* Daedalus abhors being at the mercy of the gods. Believes the best civilization is one created by man and man alone.
* Daedalus has a secret exit from his tower to his Labyrinth.
* The Labyrinth is actually a city in the plane of Mechanus.
* Daedalus has partnered with Flickerhouse Industries to secure supplies to build this massive city.
* Daedalus needs a group of adventurers to beat the maze and secure his teleport.

### Defense
* For stealing time: "Do we not all trade years of our lives for work and progress?" 
# History
* Involved in the Minotaur scandal
* Recently imprisoned in the Knossian tower