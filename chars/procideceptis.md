<!-- TITLE: Proci Deceptis -->
<!-- SUBTITLE: An elven wizard and distinguished illusionist -->

# Proci Deceptis
* A high elven wizard
* Known as "The Charlatan"
* Sigil is a half moon in a hidden full moon

# Spells
* Can produce powerful illusions that are real if the observer believes

# Relations
* A member of the [Gaia Initiative](/orgs/gaia_initiative)
* A member of the [Despotate of Morea](/orgs/despotate_of_morea)

# Goals
* Works with [Silenus](/chars/silenus) to produce the powerful voodoo dolls
* Wants to destroy [Atlantis](/orgs/atlantis)