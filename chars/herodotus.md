<!-- TITLE: Herodotus -->
<!-- SUBTITLE: Famous Historian of Athens -->

# Herodotus
* [See Wikipedia Doc](https://en.wikipedia.org/wiki/Herodotus)

# Relations
* Information about alleigance / organizations

# Land/Location
* Currently living in [Athens](/places/morea/athens)

# Goals