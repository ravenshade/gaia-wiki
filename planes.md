<!-- TITLE: Planes -->
<!-- SUBTITLE: The Planes of the Universe -->

# Planes
* Planes are large areas of the universe that are parallel to each other.
* The gods have control over most of the inner planes. Some of the outer planes.
* See [D&D Planes](https://en.wikipedia.org/wiki/Plane_(Dungeons_%26_Dragons))

# Known Planes
* Material
* Underworld
* Olympia
* Clockwork

# Mechanus
![Mechanus](/uploads/mechanus.png "Mechanus")

The clockwork plane is supposedly entirely created by the forces of mankind, gnomes, and dwarves.