<!-- TITLE: Clockwork Spider -->
<!-- SUBTITLE: A quick summary of Clockwork Spider -->

# Clockwork Spider
<img src="https://live.staticflickr.com/3737/13344997034_b755b80195_b.jpg" width="320" />

* Tiny, insect-like.
* Runs around underground of Daedalus' Labyrinth. Repairs small damage.
* Contains lethal venom from the basilisk.

# Stats
* 20 hp

## Attack
* Bite: +6 hit, 1d4 piercing + 2d6 poison
* Attach: +6 hit. Considered a grapple.
* Inject: +8 hit, 8d6 poision, chance of paralysis (see basilisk venom). Requires grapple. 