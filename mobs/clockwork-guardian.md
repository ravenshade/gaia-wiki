<!-- TITLE: Clockwork Guardian -->
<!-- SUBTITLE: Knossos Maze Guardians -->

# Clockwork Guardian
<img src="https://i.imgur.com/WFBDLq9.png" width="320" />

* Weak spot in the back. Contains Universal Time Connector.
* Powered by clockwork machinery and magic.

# Stats
* 80 hp, AC 17

## Attack
* Punch: +8, 2d10
* Shoot: +8, 3d10
## Abilities
* Explode on death: 4d12 lightning in 15ft radius