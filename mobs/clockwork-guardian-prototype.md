<!-- TITLE: Clockwork Guardian Prototype -->
<!-- SUBTITLE: Experimental, Dangerous -->

# Clockwork Guardian Prototype
<img src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/eca60872-17e0-482f-a609-b7303cc5a83d/d14vjs8-5654a841-8354-4f93-adc9-c3498a32425e.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2VjYTYwODcyLTE3ZTAtNDgyZi1hNjA5LWI3MzAzY2M1YTgzZFwvZDE0dmpzOC01NjU0YTg0MS04MzU0LTRmOTMtYWRjOS1jMzQ5OGEzMjQyNWUuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.CWL9kEX9A9uIbmytOq60KfdM2y6I6RJj6LuTpm8pq8Q" width="320" />

* Experimental. Buggy.
* Old. Rusting.