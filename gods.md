<!-- TITLE: Gods -->
<!-- SUBTITLE: The Greek Gods of Gaia -->

# Major Gods
* [Poseidon](/gods/poseidon) - the sea
* [Hades](/gods/hades) - the underworld
* [Apollo](/gods/apollo) - truth, music, poetry, art, medicine, oracles
* [Athena](/gods/athena) - wisdom, courage, inspiration, civilization, law and justice
* [Zeus](/gods/zeus) - the sky, thunder
* [Artemis](/gods/artemis) - wilderness, hunt, virginity
* [Hecate](/gods/hecate) - witchcraft, magic, sorcery


# Minor Gods
* [Dolos](/gods/dolos) - trickery
* [Ares](/gods/ares) - war
* [Persephone](/gods/persephone) - vegetation, grain, (beauty)
* [Hermes](/gods/hermes) - messenger god
* [Pan](/gods/pan) - wild, shepards, flocks
* [Hephaestus](/gods/hephaestus) - blacksmith, metallurgy, craftsmen, artisans, fire
* [Dionysus](/gods/dionysus) - wines, brew, merriment

# Titans
* Cronus - King of Titans & Time
* Gaia - Queen of Earth
* Uranus - Sky
* [Eros](/gods/eros) - Love
* Prometheus - (forethought) gave mankind knowledge
* Epimetheus - (afterthought) created all animals
* Pandora - first woman, had a box with all sorrows as punishment for mankind