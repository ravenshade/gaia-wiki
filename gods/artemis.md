<!-- TITLE: Artemis -->
<!-- SUBTITLE: A quick summary of Artemis -->

# Artemis
* Goddess of the hunt, wilderness, wild animals, and chastity
* The twin sister of [Apollo](/gods/apollo)
* Murdered her lover, Orion the great huntsman, who now resides upon the stars.

# Alleigances
* [Nymphs](/races/nymph) - mostly those near forests
* [Dryadds](/races/dryad) - almost exclusively under her bidding, she is friendly to them
* [Cyclops](/races/cyclops) - respect and build weapons for her

# History
## Artemis' Wishes
Artemis made a wish to Zeus during her early childhood. 
* to always remain a virgin
* to have many names to set her apart from her brother Apollo
* to have a bow and arrow made by the Cyclops
* to be the Phaesporia or Light Bringer
* to have a knee-length tunic so that she could hunt
* to have sixty "daughters of Okeanos", all nine years of age, to be her choir
* to have twenty Amnisides Nymphs as handmaidens to watch her dogs and bow while she rested
* to rule all the mountains
* any city
* to have the ability to help women in the pains of childbirth