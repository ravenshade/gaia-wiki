<!-- TITLE: Ariadne Nop -->
<!-- SUBTITLE: Nop & Demigoddess of Wine, Labryinths, Fertility, and Passion -->

# Ariadne Nop Knossos
<img src="/uploads/ariadne.png" width="320" />

* Daughter of [King Minos](/chars/king-minos) & Pasiphae
* Will be married to [Dionysus](/gods/dionysus)
* Pretending to be in love with [Theseus](/chars/theseus)
* Half-elf, Half-dwarf
* The only Taurus Sorceress (similar to Draconic, but for a magical bull)
* Nop's body is forever next to Chetty in Daedalus' Labyrinth

# Traits
 * Demigod, her spellcasting ability comes from the Great Bull given to her father.
 * Pugnacious, mischevious, a bit overconfident. Slutty. Ambitious. Passionate.
 * She has a flair for style and strong opinionated taste.
 * Close with Dionysus, who has a prophecy to marry her when she is strong enough.

# Dionysus-Dolos Morph
* Ariadne/Nop swap are in the same body. Only one of the two is active at once.
* Memory, mana (spell pool), and level are shared between the two. Class-based powers change based on who is active and are not shared.
* Has crossovers between classes - can use force magic to play music from a distance with minor telekinesis.
* **Swap**: An act of dionysus - chicanery, getting drunk, or sleeping with someone. The active consciousness will change after the act is complete. A swap cannot occur more than once per three days. 
* **Passive**: Ariadne/Nop can exchange whatever messages they want internally. Personalities/consciousnesses are not combined.
# Sheet
* Female Dwarf/Half Elf, Lv ? Taurus Sorceress
* Str 12 (+1) Dex 9 (-1) Con 16 (+3) Int 20 (+5) Wis 7 (-2) Cha 18 (+4)

## Inventory
* [Staff of Dionysus](/items/staff-of-dionysus) - Legendary artifact given to her
* [Endless Flaming Goblet](/items/endless-flaming-goblet) - From her father

# Friends
 * [Lydia](/chars/lydia-marcheschi) - closest friend since childhood. Competitive friendship.
 * [Thea](/chars/thea-thunderbrew) - good friend who is as stubborn as she is. 
# History & Plans
* Ariadne is conflicted on what to do about Asterion. Learned about Daedalus' secret plans to leave and wanted to kill him for what he did to Knossos. 
* Prophecy when she was born that she would marry Dionysus and become a Goddess. King Minos was thrilled and converted the city to worship him. She can aspire to become a goddess.

## Thoughts on Dionysus Marriage
 * She wants the power of being a goddess. She's gotten used to the idea and thinks she can rise to it.
 * However, she thinks Dionysus is a weak-willed god and wastes his power. She keeps this a secret.

## Thoughts on Throne of Knossos & Swap
 * Ariadne loves power. The idea of being the queen of knossos is alluring. Unfortunately, she knows that a life of ruling a nation would stifle her quest to become a powerful sorceress and aspire to be a Goddess.
 * Dionysus gifted her father the Staff of Dionysus to give to her when she first felt her power. Her power was honed by Daedalus through teaching, but she realized his trainings actually restrained her growth.
 * Ariadne realizes that gaining the power of a bard, even partially, is good for her quest. She also knows that Dionysus respected Nop as a patron of a The Heady Topper and musicmaker. She offered foregoing her middle name to be 'Nop.'
 * Ariadne likes the quest for pillars since it seems befitting of a person destined to be a goddess. She believes if she were to succeed, not only would it provide a lot of power, it would also provide a massive feather in her cap.