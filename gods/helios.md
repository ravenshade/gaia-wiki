<!-- TITLE: Helios the Titan Sun -->
<!-- SUBTITLE: Helios the Titan Sun -->

# Helios
 * Titan of the Sun
 * Trapped inside a blade named [Sol](/items/sol)
 * Desires revenge on Poseidon and Zeus for trapping him.