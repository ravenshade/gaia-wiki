<!-- TITLE: Hermes -->
<!-- SUBTITLE: God of trade, heraldry, merchants, commerce, roads, sports, travelers, and athletes -->

# Hermes
* Emissary and Messenger of the Gods
* In charge of escorting souls to the [Underworld](/places/underworld)