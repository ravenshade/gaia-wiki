<!-- TITLE: Ariadne -->
<!-- SUBTITLE: Goddess of Wine, Labryinths, Fertility, and Passion -->

# Ariadne
<img src="/uploads/ariadne.png" width="320" />

* Daughter of [King Minos](/chars/king-minos) & Pasiphae
* Will be married to [Dionysus](/gods/dionysus)
* Pretending to be in love with [Theseus](/chars/theseus)
* Half-elf, Half-dwarf
* The only Taurus Sorceress (similar to Draconic, but for a magical bull)
* See [Ariadne-Nop](/gods/ariadne-nop)

# Traits
* Wild-child, loves to have fun, slutty, mischevious. 
* Good fashion sense (Aphrodite even says so)
* Naive but doesn't think she is, thinks the world caters to her.
* Power-hungry, cocky, but easily frightened.

# Sheet
* Female Dwarf/Half Elf, Lv 13 Taurus Sorceress
* AC 13, 140 HP, 30 ft, prof +5
* Str 12 (+1) Dex 9 (-1) Con 16 (+3) Int 20 (+5) Wis 7 (-2) Cha 18 (+4)

## Features
* Elemental Affinity: Whenever doing force damage, cha mod is added.
* Bull Horns (13th): Sprout bull horns and triple movement. Anyone caught in path makes DC (spellcast) save or be knocked prone. Lasts 1m.
* Metamagic: Distant Spell, Quickened Spell, Twinned Spell. (13)
* Affinity: Most of her magic causes force damage. Characterized by small bulls running at enemies.
## Spells
* Cantrips: Firebolt, Mage Hand, Message, Presti, Encode Thoughts, Shove: ft 30 4d10 force damage pushes back 5ft
* 1st: Mage Armor, Silent Image, Disguise Self, Comprehend Languages, Identify
* 2nd: Invisibility, Misty Step, Mirror Image
* 3rd: Fireball, Counterspell, 'Make Drunk' (from Dionysus)
* 4th: Dimension Door, Polymorph, Wall of Fire, Dominate Beast
* 5th: Scrying, Telekinesis, Animate Objects
* 6th: Geas, Mass Suggestion, Chain Lightning (Bull Stampede)
* 7th: Reverse Gravity


# Friends
 * [Lydia](/chars/lydia-marcheschi) - closest friend since childhood. Competitive friendship.
 * [Thea](/chars/thea-thunderbrew) - good friend who is as stubborn as she is. 
# History & Plans
* Ariadne is conflicted on what to do about Asterion. Learned about Daedalus' secret plans to leave and wanted to kill him for what he did to Knossos. 
* Combined consciousnesses with [Nop](/chars/nop).