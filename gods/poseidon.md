<!-- TITLE: Poseidon -->
<!-- SUBTITLE: The God of the Sea -->

# Poseidon
* Powerful water magic

# Machinations
* [The Atlantic War](/plans/atlantic_war)
* [The Destruction of the Pillars of Hercules](/plans/pillars_of_hercules_destruction)

