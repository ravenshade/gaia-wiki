<!-- TITLE: Persephone -->
<!-- SUBTITLE: A quick summary of Persephone -->

# Persephone
* [See Wikipedia Doc](https://en.wikipedia.org/wiki/Persephone)
* Wife of [Hades](/gods/hades)
* Personification of vegetation, grain, harvest
* Can only leave Underworld when Gaia is blooming/blossoming because of eating pomegranate in [the Underworld](/places/underworld)