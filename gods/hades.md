<!-- TITLE: Hades -->
<!-- SUBTITLE: The god of the underworld -->

# Hades
* [See wikipedia doc](https://en.wikipedia.org/wiki/Hades)
* Married to [Persephone](/gods/persephone)

# Machinations
* [Atlantic War](/plans/atlantic_war)
* [Construction of pillars of hercules](/plans/pillars_of_hercules_construction)