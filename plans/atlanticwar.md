<!-- TITLE: Atlantic War -->
<!-- SUBTITLE: Plan to wage war on the Land -->

# Motives
* [Poseidon](/gods/poseidon) was angered by the [prophecy](/events/prophecy_of_athens) and wished to prove it wrong.
* Poseidon also wanted to take over the land and mortal realms for rejecting him.

# Actions
* Poseidon destroyed the [Hanging Garden](/events/hanging_gardens_destruction)
* Poseidon ensured the only witness, [Odysseus](/chars/odysseus), could not return home.
# Plans
* Poseidon plans to destroy land through the [Pillars of Hercules destruction](/plans/pillars_of_hercules_destruction)