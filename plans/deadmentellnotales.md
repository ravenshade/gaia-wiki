<!-- TITLE: Dead Men Tell No Tales -->
<!-- SUBTITLE: A plot between Hades and Poseidon -->

# Agreement Terms
* Poseidon will supply Hades with a large amount of dead souls from Sailors during the [Delimore War](/events/delimore_war)
* Hades will give Poseidon undead soldiers for his [Atlantic War](/plans/atlantic_war) plans.