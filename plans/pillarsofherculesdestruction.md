<!-- TITLE: Pillars of Hercules Destruction -->
<!-- SUBTITLE: Poseidon plans to destroy the pillars of hercules -->

# Plot
* [Pillars of Hercules](/places/pillars_of_hercules) hold up the lands of [Delia](/places/delia) and [Morea](/places/morea)
* Poseidon wants to sink them into the sea.

# Progress
* The first pillar was destroyed under the Hanging Gardens
* The second pillar was destroyed under Gelatin
* The third pillar was mostly destroyed in the Morean Tip