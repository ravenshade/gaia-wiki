<!-- TITLE: Gelatin Kingdom -->
<!-- SUBTITLE: Home to Queen Anne, King Kenneth -->

# Location
* The Kingdom is on the pinnacle of [Island of Gelatin](/places/gelatin)
# Royal Family
* [King Kenneth](/chars/king_kenneth) (king)
* [Queen Anne](/chars/queen_anne) (queen)
* [Princess Elizabeth](/chars/princess_elizabeth) (princess)
* [Princess Madison](/chars/princess_madison) (princess)
* [Prince Luke](/chars/prince_luke) (prince)

# Royalty
* [Sir Noah the Wizard](/chars/sir_noah)
* [Sir Blake the Wizard](/chars/sir_blake)
* [Sir Edward the Champion](/chars/sir_edward)
* [Stuart the Herbalist](/chars/herbalist_stuart)

# Soldiers
* [Guard Thomas](/chars/guard_thomas)

# Notable Citizens
* [Brandy](/chars/brandy) the Witch (farms)

