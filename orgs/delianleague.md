<!-- TITLE: The Delian League -->
<!-- SUBTITLE: A quick summary of The Delian League -->

# Crest
<img src="/uploads/delian-league.png" alt="Delian League" width="250px" />

# Land
* [Apol](/places/delia/apol) (capital)
* [Delphi](/places/delia/delphi)
* [Olympia](/places/delia/olympia)
* [Knossos](/places/morea/knossos)
* [Babylon](/places/babylon)
* [Delos](/places/delos)
* [Port City of Patras](/places/patras)
* [Sparta](/places/delia/sparta)

# The League
## Military
* See [military structure](/notes/military_structure)
* [General Rockfurt](/chars/general-rockfurt)
### Leaders
### Army & Cavalry
* [King Leonidas](/chars/leonidas)
* [King Minos](/chars/king-minos) (dead)
### Navy
* [King Periphas](/chars/periphas)
* [Candora Aristocracy](/orgs/candora_aristocracy)
### Siege
* [King Phillip](/chars/phillip)
### Magistrate
* [King Nebuchadnezzar](/chars/nebuchadnezzar)
# Government
* Feudal, Monarchy
* Hierarchy

# Populace
* Human (most common)
* Dwarf (frequent)
* Giant
* Elves (rare)
* Gnomes (common)
* Halflings
* Goliaths (Sparta)

# History
* Sparta joins the Delian League in Late Summer, Year 218
