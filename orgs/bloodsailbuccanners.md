<!-- TITLE: Bloodsail Buccanners -->
<!-- SUBTITLE: A quick summary of Bloodsailbuccanners -->

# Members
* [Hairy Finly Barbadour](/chars/finly_barbadour) (captain)
* Sam Gale - pirate
* Jeffrey the Halfling - recruiter

# History
* Destroyed Arborvale Scoundrels
* Destroyed Fittenship Fiddlers
* Was destroyed in 214
* Known for chopping off fingers and launching them
* Founded [Pirates Cove](/places/gelatin/pirates_cove)