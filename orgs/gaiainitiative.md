<!-- TITLE: Gaia Initiative -->
<!-- SUBTITLE: Gaia Initiative is an organization formed by Hades to stop Poseidon-->

# Members
* [Duplicamos](/chars/duplicamos)
* [Proci Deceptis](/chars/proci_deceptis)
* [Silenus](/chars/silenus)
* [Stheno](/demigods/stheno)
* (covertly) [Azul](/chars/azul)

# Slaves
* [Hercules](/demigods/hercules)
* [Krazzak](/chars/krazzak) (former)

# Plans
 * Hades is attempting to subvert Poseidon, but above-board made a deal with him.

 * Hades plans to stop destruction of the pillars of hercules (Duplicamos Apprentice)

 * Hecate plans to form an army of dead to take over the land of living (Azul)

 * Hades plans to control the living and destroy Atlantis (Proci Deceptis, Silenus)

 * Hades has a spy on Poseidon (Stheno)
# Goals
* [Control the Population of Morea](/plans/morea_mind_control)
* [Construct new Pillars of Hercules](/plans/pillar_of_hercules_construction)
* Stop the [Destruction of the Pillars of Hercules](/plans/pillars_of_hercules_destruction)