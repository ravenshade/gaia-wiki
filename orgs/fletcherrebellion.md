<!-- TITLE: The Fletcher Rebellion -->
<!-- SUBTITLE: Artem's saving grace -->

# Fletcher Rebellion
# Members
* [Alleria](/chars/alleria) - leader of the rebellion
* Orlov - elven leader, previous bowyer
* Qiren - elven wizard (lv 2)

# Operation 
* Cause fire in smaller tree, use distraction to steal bows that are locked up
* Smaller task force attempts to assassinate General
* Paid Jeremy to spy on the [party](/party)
* Shut down in order to pay for [Jeremy's](/chars/jeremy) resurrection.