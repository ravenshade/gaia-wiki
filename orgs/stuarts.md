<!-- TITLE: Stuarts -->
<!-- SUBTITLE: A collection of accidentally duplicated Stuart -->

# The Stuarts of Gaia
* [Stuart the Herbalist](/chars/herbalist_stuart)
* [Stuart the Alchemist](/chars/alchemist_stuart)
* [Stuart the Tortle](/chars/tortle_stuart)
* [Stuart the Bard](/chars/bandu-troupe)

# History
* Accidentally duplicated by the powerful sorcerer Master [Duplicamos](/chars/duplicamos)