<!-- TITLE: Seven Sages of Gaia -->
<!-- SUBTITLE: A group of philosophers that are renowned for wisdom -->

# Members
* [Thales](/chars/thales) - "Know thyself"
* Pittacus - "Destroy Nobility"
* Bias - Legislator of republics
* Solon - Famous legislature, wants to undermine [Plato](/chars/plato)
* Periander
* Anacharsis the Scythian
* Chilon - The militarizer of Sparta

# Goals
* Seek to undermine [The Republic](/orgs/platos_republic)