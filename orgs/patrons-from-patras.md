<!-- TITLE: Patrons From Patras -->
<!-- SUBTITLE: A quick summary of Patrons From Patras -->

# Patrons From Patras
A knock-off group that attemps to follow the party and take credit for their deeds.

# Party
- Adrian Golgorroth (human ranger) the Hawkeye
- Theed Latto (kenku warlock) the Liar
- Kiwi (kenku monk) the Coward

## Former Members
- (dead) Carrie (elven bard) the Countess
- (dead) James Bonnett (human rogue) the Sly

# History

## Kor Kash Crash
 * Carrie the elf was killed by the Patriots of Patras after she attempted to flee
 * James Bonnett was killed by Emmett Baldier for his attempt to steal the crown
 * Theed & Kiwi were able to escape from the Patriots.