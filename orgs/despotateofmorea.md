<!-- TITLE: Despotate of Morea -->
<!-- SUBTITLE: A quick summary of Despotate of Morea -->

# Crest
<img src="/uploads/despotate-of-morea.png" alt="Despotate Of Morea" width="250px" />

# Land
* [Athens](/places/morea/athens) (capital)
* [Corinth](/places/morea/corinth)
* [Argos](/places/morea/argos)
* [Cecily](/places/cecily)
* [Artem](/places/artem) (recently annexed, rebellious)

# The Despotate
## Military
* See [military structure](/notes/military_structure)

### Leaders

### Army & Cavalry
* **General**: [Aegeus](/chars/aegeus) 
* **General**: [Pericles](/chars/pericles) 
* **General**: [Aristides](/chars/aristides) 

### Navy
* **Fleet Admiral**: [Conon](/chars/conon)
* **Admiral**: See [Athenian Admirals](https://en.wikipedia.org/wiki/Category:Ancient_Athenian_admirals)

### Siege
* **General**: [Archimedes](/chars/archimedes)

### Magistrate
* **Archmage**: [Thales](/chars/thales)
* **Sage**: [The Seven Sages of Gaia](/orgs/seven_sages_of_gaia)
* **Magister**: [Magister Tossos](/chars/magister-tossos), etc.

# Inhabitants
* Elves
* Halflings
* Humans
* Dwarves

# Ongoing Conflicts
* [The Delimore War](/events/delimore_war)

## Advantages
* Higher population
* More advanced engineering
* More advanced magic

## Disadvantages
* Council makes decisive planning difficult

# History
* Originally formed by Corinth, Athens, and Argos.