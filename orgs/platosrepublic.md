<!-- TITLE: Plato's Republic -->
<!-- SUBTITLE: The first republican government -->

# Location
* The Republic is an eminent part of the [Despotate of Morea](/orgs/despotate_of_morea)
* The Republic is hosted in [Athens](/places/morea/athens)

# Senators
## Athens
* [Socrates](/chars/socrates)
* [Plato](/chars/plato)
* [Aristotle](/chars/aristotle)

## Corinth
* [Herodotus](/chars/herodotus)
* [Periander](/chars/periander)