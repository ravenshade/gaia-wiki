<!-- TITLE: Cult of Poseidon -->
<!-- SUBTITLE: Poseidon's followers -->

# Cult of Poseidon

## Members
 * [Duplicamos (Master)](/chars/duplicamos)
 * (Unknowingly) [Lucius Creed](/party)
 * [Atlanti people](/orgs/atlantis)

## Plans
 * Duplicamos (Master) intends to become Poseidon by dethroning him, turning the gods against him, stealing his power.

 * Duplicamos captured Zeus' harem and is actively using it to keep Zeus in check from doing anything about his plans.

 * Duplicamos approached Poseidon, had Zeus admit that he slept with Poseidon's wife, and Poseidon was furious.

 * Poseidon plans to get back at Zeus by sinking parts of the material realm with the Cult of Poseidon & Atlanti People

 * Poseidon & Duplicamos started a war between Delia and Morea to blame his pillar destruction plan.

 * Poseidon made a deal with Hades to give him power/souls if he does not interfere with his revenge

 * Duplicamos formed the Cult of Poseidon to carry out the [destruction plan](/plans/pillarsofherculesdestruction)

### Zeus' Harem (secret)
Duplicamos has slowly been capturing all of Zeus' harem. He is using this information to blackmail Zeus into being a bystander.

They are in trapped in prisons in the lands of the [Scar of Zeus](/places/morea/scar-of-zeus)

 * [Queen Alleria of Artem](/chars/alleria)
 * 

### Unleash Helios (secret)
When Apollo was young and wanted to prove his strength by rebelling against Zeus, the titan Helios forged him a weapon named [Sol](/items/sol).

Apollo, with his hubris, lost the battle by losing the weapon. As the weapon fell into earth, it became the first falling star.

When the weapon landed, it destroyed swath of Morea. Poseidon, who wanted Zeus and Selene to rule the sky (since they favored him), decided
to lock up the titan for conspiring against them. 

Poseidon trapped Helios inside the blade that fell from the sky. 

Master Duplicamos intends to unleash Helios, who desires revenge against Poseidon and Zeus. 

Master Duplicamos knows where Sol is hidden, protected in the Straits of Gibraltar.

### Destroy the Pillars of Hercules
The cult wants to sink part of the material realm into the ocean to punish Zeus and Hera. 

* With Poseidon's help, Duplicamos learned of the location of [Athenas Library](/places/morea/arcadia/athenas-library).
* Poseidon assisted with destroying the blessing of Hera with assistance of Hecate.


### Unleash Charybdis & Scylla
The cult wishes to unleash these great beasts to destroy Apol and Athens near the end of the war.

Duplicamos desires the staff to gain control of Poseidon's power.

The ritual to unlock them requires...

 * Night (nighthouse)
 * Staff of Selene - stolen by sorcerer who knew plans, Lucius got it back for them by assaulting Cecily.
 * Star of Selene - hidden in the Temple of the Moon, retrieved when Patriots failed to stop them
 * Life Sacrifice

