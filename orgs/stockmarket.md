<!-- TITLE: Stock Market -->
<!-- SUBTITLE: The Exchange of Goods across Athens -->

# Stock Market
A place to buy stock in certain goods, organizations, and cities along the Republic. Matched to current market.

Loose based on US stock market + rolls. 
# Market
* wheat - AGFS - 2g ($4) -> 300g 
* weapons - exxon - 17g ($78)
* fruit - 3g
* athens - US bond - 182g
# History
* Started in year 180
* Managed by the [Candora Aristocracy](/orgs/candora_aristocracy)

# Trades
## Quadarius
* 1 x weapons