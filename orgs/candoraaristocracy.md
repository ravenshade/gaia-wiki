<!-- TITLE: Candora Aristocracy -->
<!-- SUBTITLE: The authoritarian rulers of Apol -->

# Location
* The Aristocracy controls [Apol](/places/delia/apol) and its surrounding area.
* The Aristocracy still reports to [King Periphas](/chars/periphas) but is beginning to gain power over him.

# Members
 * [Lord Lysander](/chars/lysander)
 * [Duchess Sabrina](/chars/sabrina)
# Orgs
* Manages the [Stock Market](/orgs/stock_market)