<!-- TITLE: Duplication Magic -->
<!-- SUBTITLE: The Ability to Duplicate Real Worlds -->

# Duplication Magic
* The ability to use a mixture of portals and illusions to produce semi pocket-spaces within a plane that appear as duplicates of a different part of the real world. 
* These duplications have an entrance and exit (through portals)
* The duplications traverse real distance.