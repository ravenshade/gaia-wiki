<!-- TITLE: Blood Magic -->
<!-- SUBTITLE: A dangerous form of magic that deals in life -->

# Basics
* Blood (or life force) can be sacrified to gain power over some set of life force.
* The life force has to make some decision to give away their control.
* Life force sacrificed can force control over choice but it is at a painful ratio.

# Control Orbs
* Huge sacrifices can be made to create control orbs.
* Control orbs allow for puppet-like control over other beings through contracts of blood
* Destroying a control orb takes a sacrifice equal to the life that it is binding.

# Interactions
* Beings can be trapped alive inside an orb of control. See [Krazzak](/chars/krazzak). These beings control the orb's puppets and decide how spells are used.
* If no being is trapped in the orb, the wielder controls both.