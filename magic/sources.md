<!-- TITLE: Sources -->
<!-- SUBTITLE: Magical Sources in Gaia -->

# Magic
The gods created order in the world. Magic is manifested in the natural order of the world because it was produced by divine power. Studying the natural world and using elements of it can allow utilization of that divine power. Generally comes from sources stronger than even the gods (titans).

# Sources

## Love
A lesser known powerful force is love, from Eros. Love magic is not often studied. The most primal of all forces.

## Time
Time was created by the great titan Cronus. Use of time provides some of the strongest most distilled magic.

## Life
Life was created by the mother earth Gaia. Life binds life, which enables dark magic such as [Blood Magic](/magic/blood) and necromancy.

## Sky
Light, Shadow, and the Sky was created by Uranus. 

## Fire
Fire is a force manifested as knowledge. It was a gift from Prometheus to humanity despite the fact they should not have it.

## Hope
Hope generally manifests in sorrows. Dangerous if not used with extreme caution. Provided as the last result of Pandora opening the box.

