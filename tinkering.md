<!-- TITLE: Tinkering -->
<!-- SUBTITLE: A quick summary of Tinkering -->

# Items
## Crafting
 * Failures to craft may result in additional time or lost materials.
 * 1 lb == "ingot"
 * 6 oz == "bag"

### Explosive Arrow
1 hr -- DC 10
6 oz. Gunpowder
1 lb Iron
Effect: 1d6 AoE 5ft radius.

### Large Bomb
6 hr -- DC 10
2 lb Iron
24 oz. Gunpowder
Rope/Fuse
Effect: After 6-24s (fuse) deal 2d6 fire damage in 10ft radius. Deals quadruple damage to structures.

### Electric Rope
18 hr -- DC 15
1 lb powdered magiquartz
1 lb Iron (10ft length)
Anyone who steps over takes 1 round stun unless they see / avoid it.

# Substances
## Magiquartz
* White (rare), Black (very rare), Red (exotic), Blue (exotic)
* Unstable (epic): Constantly weaves in and out of different elemental states.
* Timeshift (legendary): Is in a fault line of time.
* ??

## Iridium
* Created to spite the Goddess of Magic
* Sucks magical energy into it. 

## Mithril
* Incredibly light but hardy material.

## Thorium
* Incredibly heavy but nearly impossible to destroy.
