<!-- TITLE: Party -->
<!-- SUBTITLE: The Patriots of Patras -->

# Members
* [Leucis Creed](/chars/leucis_creed) - Warlock (Poseidon) - Tiefling (Undead) PP 14 - "the Honest"
* [Emmett Baldier](/chars/emmett_baldier) - Rogue (Arcane Trickster) - Dwarf - PP 15 - "the Quick"
* [Quadarius](/chars/quadarius) - Ranger (Colossus Slayer) - Human - PP 16 - "the Merciless"
* [Pina Corona](/chars/pina) - Monk - Pianta - PP 16 - "the Steadfast"
* [Barnabus Bluar](/chars/barnabus) - Bard - Half Elf - ???

# Former Members
* A mermaid girl - disappeared
* [Queen Anne](/chars/queen_anne) - Fighter - at castle
* [Kreios Epimetheus](/chars/kreios) - Wizard - tournament
* [Seraphina Libera](/chars/seraphina) - Fighter - Gelatin called for aid
* [Billy Bob](/chars/billy_bob) - Ranger (Beast) - coconut shop in a swamp
* [Amber Fireforge](/chars/amber_fireforge) - Cleric - Swallowed by ocean
* [Nop](/chars/nop) - Glamor Bard - PP 14
* [Daru](/chars/daru) - Moon Druid
* [Ariadne Nop](/gods/ariadne-nop) - Bard/Sorceress  - "the Queen of Knossos" - Left to save Knossos.

# Titles
 * Navy crew of Delian League (Lucius is Captain)
 * Knights of Knossos

# History
* See [summary of events](/summary)
* Formerly the 17th naval divison of the Delian League