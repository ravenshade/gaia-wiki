<!-- TITLE: Hercules -->
<!-- SUBTITLE: A demigod born of Zeus with great strength -->

# Hercules
* A half human half God man

# Abilities
* Amazing strength
* Only one who can break or build a [Pillar of Hercules](/places/pillars_of_hercules)

# Family
* Mother: [Alcmene](/chars/alcmene)
* Father: [Zeus](/gods/zeus)

# Relations
* Currently slave of the [Gaia Initiative](/orgs/gaia_initiative)

# Accomplishments
* Killed Cerberus and stole one of his [fangs](/items/cerberus_fang).