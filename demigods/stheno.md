<!-- TITLE: Stheno -->
<!-- SUBTITLE: A naga demigod changeling -->

# Stheno
* A [naga](/mobs/naga) woman with many heads 
* Known as "Sister of Medusa"
* Sigil is a swirly fish

# Spells
* Can change to whatever appearance she desires
* The [power of a siren](/magic/siren)
* Can alter people's destinies

# Relations
* A member of the [Gaia Initiative](/orgs/gaia_initiative)
* A member of [Poseidon's](/gods/poseidon) [Court](/orgs/atlantis)

# Goals
* Wants to destroy [Atlantis](/orgs/atlantis)
* In charge of intelligence for the Gaia Initiative

# History
* Their mother was raped by Poseidon, as was [she and here sisters](/events/raping_of_stheno_medusa).