<!-- TITLE: Hormes the Black Dragon - Hoard -->
<!-- SUBTITLE: A quick summary of Hormes Hoard -->

# Items
 * 1,093 P,  1,230 G,  20 S
 * Spies' Murmur
 * [Ring of Cowards](/items/ring-of-cowards) - assist Emmett in tank/rogue
 * [Galder's Bubble Pipe](http://5etools.ravenshade.com/items.html#galder's%20bubble%20pipe_llk)
 * [Selene's Grace](/items/selenes-grace)
 * Black Dragon's Claw (required for brewing [Kingkiller's poison](/items/king-killer-poison))
 * Dwarven Artifacts worth 3,678 G
 * A jeweled crown worth 300 G
 * King Kor's Crown (Diamond worth 500 G)
 * 1000 G ruby
 * 500 G sapphire
 * 100 G agate
 * A journal from the Kor King with notes that details the location of Hermes' [Winged Helmet](/items/winged-helmet), [Eros' Bow](/items/eros-bow), [Selene's Star](/items/selenes-star)


# Ideas
 * Elune's Grace - allow giving CR 0 / CR 1/8 to another player druid-only