<!-- TITLE: Shops -->
<!-- SUBTITLE: Random shop notes -->

# Generic
## Inn Names
        The Crew's Crockpot
        The Beer-battered boar
        The Waterlogged Barrel
        The Buxom Wench
        The Oak and croiser
        Escher's Last Stair
## Stores
        The Satyr's Stiletto
        The Slippery Eel
        The Porter's Plumpkins
        The Poisoned Peach
        The Stinky Turnip
        The Goblin's Gullet
## Magic Stores
        Runic Rods & Haberdashery
        Wart on the Nose Wands
        Bits, Bats, and Boils
        Hippocamphumprus
        Pea-sized Pegasus
## Blacksmiths
        Forge of Fortune
        So Hot we Blue-it!
        The Molten Mashers
# Aquatic
## Inn
        The Fiddler
## Potion Shops
        The Siren's Call
## Weapon Shops
        Tridents & Trinkets
## Misc Shops
        Trunkets & Trumpets
				
# Forest
## Inn
Tree's Cradle
## Weapon Shops
Truestrike Armory
The Last Defense

