<!-- TITLE: Races -->
<!-- SUBTITLE: Races/Beasts ideas -->

## Reaper
* Gift from Persephone to Hades
* Collects souls that refuse to go with Hermes because they aren't buried/ready and known Charon will turn them away
* Reapers have a long scythe, float, wear a "dementor robe" and have a giant mouth. 
* Mostly lurk around war zones