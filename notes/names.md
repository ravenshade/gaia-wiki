<!-- TITLE: Names -->
<!-- SUBTITLE: Random Names -->

# Greek
Eros Siskas
Nikolaos Pennas
Aris Panagopoulos
Spyro Kostopoulos
Platon Rigas
Kyriakos Boosalis
Filippos Kritikos
Makis Petras
Platon Pardos
Tassos Thanos
-----------
Marietta Kanelou
Iris Andri
Efthimia Manika
Sappho Palla
Dorothea Mella
Filareti Hana
Theano Kosta
Garifallia Zenou
Agathi Karra
Roxanni Ioannou

# Demon
Brostranas
Tralvannath
Gorthreroz
Uzgan
Mogthomod
Rogthollen
Tagdrun
Margranoz
Targellir
Brurrun