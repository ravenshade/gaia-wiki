<!-- TITLE: Labyrinth Items -->
<!-- SUBTITLE: A quick summary of Labyrinth Items -->

# Items
* Tome of Leadership & Influence
* [Unstable Whirrling Vat](/items/unstable-whirrling-vat) - ("goblin" chamber) produces 1 gallon rocket fuel / day
* [Capacitor Gloves](/items/capacitor-gloves) - "Ring of Lightning Strikes"
* Instrument of the Bards, Doss Lute
* Storm Boomerang
* [Gyro-balanced Blunderbuss](/items/gyro-balanced-blunderbuss) - +1 hit, 2d10, reload, 80/240 (no attune)
* Boots of Striding and Springing
* Headband of Intellect
* [Finnicky Timepiece](/items/finnicky-timepiece) - constantly ticks, allows user to cast slow (3rd) once per day. Attunement.
* Ring of Truth Telling
* Belt of hill Giant Strength
* Frost Brand - Longsword
