<!-- TITLE: Daedalus Labyrinth Notes -->
<!-- SUBTITLE: For quick work -->

Daedalus' Labyrinth

Ideas
    Layers
        Hedge Maze (changes dynamically)
            Prevents flying: dangerous/poisonous hedge, grows rapidly when touched
            The middle cannot be reached from the exterior
        Underground Clockworks (with gears)
    
    Challenges:
        mirror challenge
        Talking Door Challenge
        Massive Gap Challenge
        Water-filling section
        The Dangerous Delight (antidote hidden elsewhere)
        Troll Bridge
    Items:
        rope of climbing + (chk) ring of jumping + robe of useful items
    Sigils:
        Can be licked, reveal secret items
    Creatures:
        The Lost Ones
            - Children that have wandered into the maze and live there
            - Have figured out a lot of the tunnels around the maze
        The Quarelling Statues
            - Living Statues near a Basalisk. Always arguing about the party.
        The Guardians
            - Constructs built by Daedalus to protect the maze
            - Will attack anyone in the Clockworks
        The Wardogs
            - Soldiers originally 'fed' to the Minotaur. Never saw him.
            - Formed a gang that took over one of the puzzle rooms as a base.
        The Gardeners
            - Little Gnomes in charge of taking care of the hedges
        The Sweepers
            - Massive terrifying devices that clean the tunnels underneath
    Timers:
        contantly - mazes randomize
        30 minutes - section rotates (garden, city, forest, mtns)
        60 minutes - grandfather clock dings (backwards)
    Rules/Logic:
        The Only way to get through the maze is to think outside the maze.
        
Overall
-------
    Haskell at front, greets, gives free garden key, rules, and a portable clock
    Gardens door doesn't have keyhole. Adventurers there have a load of keys tossed about.
        Key to gardening -- sunlight, water, soil.
        