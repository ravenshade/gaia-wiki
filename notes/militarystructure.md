<!-- TITLE: Military Structure -->
<!-- SUBTITLE: Structure of Militaries in Gaia -->

Military in most Gaia forces falls into five major categories. Usually a collection falls under a single leader.

# Leaders
* Supreme Commander - highest ranking official

# Documentation
* DMID or DMID - Despotate Military ID, Delian Military ID
# Army & Cavalry
* General
* Brigadier
* Colonel
* Battalion Leader
* Captain
* Hoplite - Horsemen - Hyparchos
# Navy
* Fleet Admiral
* Admiral
* Commodore
* Captain
* Commander
* Lieutenant
* Boatswain, Purser, Lieutenant, Carpenter
* Sailor
# Siege
* General
* Brigadier
* Siege Master
* Siege Engineer
* Loader, Rigger, Worker
# Magistrate
* Archmage
* Sage, Occultist
* Magister, Magus, Alchemist, Engineer
* Wizard, Warlock, Sorceror, Magician, Witch, Mage, Conjuror, Enchanter, Evoker