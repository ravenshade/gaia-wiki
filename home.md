<!-- TITLE: Gaia -->
<!-- SUBTITLE: A world where Greek Gods & Goddesses are real. -->

# Gaia

The world [Gaia](/places/gaia) is made up of [Delia](/places/delia), [Morea](/places/morea), [Atlantis](/places/atlantis), etc.

The [Delian League](/orgs/delian_league) and the [Despotate of Morea](/orgs/despotate_of_morea) are vying for control over the [Gaian Sea](/places/gaian_sea) and the two major continents. [Poseidon's](/gods/poseidon) [Atlanti](/orgs/atlantis) people, hailing from [Atlantis](/places/atlantis) in the [Atlantic Sea](/places/atlantic_sea) are plotting to takeover [Morea](/places/morea) through destruction of the [Pillar of Hercules](/places/pillar_of_hercules) by the [Cult of Poseidon](/orgs/cult-of-poseidon) but are contested by a secret cult between mortals and [Hades](/gods/hades) known as the [Gaia Initiative](/orgs/gaia_initiative). 

The [party](/party) is attempting to handle the situation.

# Status
* Year 219
* Season changes constantly based on the God's opinions
* [Stock Market](/orgs/stock_market)

# Templates
* [Organizations](/orgs/template) - groups, governments
* [Places](/places/template) - locations, nations
* [Chars](/chars/template) - characters

# For upkeep
 * [/notes/](/notes/)
 * [/plans/](/plans/)
 * [/party/](/party/)
 * [/summary](/summary/)
 * [/accents](/accents)

# Structure
 * /places/ (hierarchical - gaia, delia, morea)
 * /events/
 * /chars/
 * [/gods/](/gods/)
 * [/demigods/](/demigods/)
 * [/races/](/races/)
 * [/magic](/magic)
 * /places/shops
 * /items/
 * /orgs/
 * /plans/