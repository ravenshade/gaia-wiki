<!-- TITLE: Notes -->
<!-- SUBTITLE: Scribbles of ideas -->

* [Shops](notes/shops)
* [Characters](notes/chars)
* [Places](notes/places)
* [Races/Beasts](/notes/races)
* [Items](/notes/items)

# scribbles
Jason & Argonauts - steal the golden fleece 
Labrynth was created by Daedalus
Theseus (former athenian king, shipman) to kill the minotaur near Knossos - kill King Minos of Crete
Alpheius in love with Artemis -- went after her nymphs
Bull-Run in city of Taurus (sends bulls to Marathon)
The Fates in Cave of Artemis
Amore - city of virgins looking for advice from Eros & nymphs
pillar of hercules destroyed under tower of babel
pillar of hercules under temple of artemis -- requires honor of Artemis
Marathon involves a bull run and long race



notes
    Argos is the home of democracy
    home of alexander the great
    Helped launch the first attack on Artem
    United with Corinth originally, later Athens, to form the Despotate
    City of Mages, competitive with Arcadia who refused to join the Despotate with them
    Founded by "The Three Kings of Argos" - powerful mages of different magic types
    Famous for citrus fruits & melons
    knossos fed athenian soldiers to minotaur
    at war with Artem, currently ruled by General Aegeus
    
    sophocles famous writer