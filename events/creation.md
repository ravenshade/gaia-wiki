<!-- TITLE: Creation -->
<!-- SUBTITLE: Creation of Gaia -->

# Overview
The world was created. Poseidon took the seas, Zeus the skies, and Hades the underworld.