<!-- TITLE: Prophecy of Athens -->
<!-- SUBTITLE: The great prophecy given by Oracle Delphi -->

# Prophecy of Athens
The prophecy, given by [Oracle Delphi](/chars/oracle_delphi) in the Temple of Apollo, predicted that Poseidon would try and fail to conquer the city of Athens.