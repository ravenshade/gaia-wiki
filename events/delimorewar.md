<!-- TITLE: Delimore War -->
<!-- SUBTITLE: A war between the Delian League and the Despotate of Morea -->

# Events
* [Delian League](/orgs/delian_league) declared war on the [Despotate of Morea](/orgs/despotate_of_morea) in the year 200 after [the destruction of the hanging gardens](/events/hanging_gardens_destruction).
* The first fights began in the [Gaian Sea](/places/gaian_sea).