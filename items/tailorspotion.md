<!-- TITLE: Tailor's Potion -->
<!-- SUBTITLE: A potion that allows the user to change their clothes -->

# Tailor's Potion
* Lasts three hours (based on dosage)
* Allows the user to create an illusion around their body of the appearance of clothes
* User must not be wearing any clothing, otherwise "clipping" will occur.