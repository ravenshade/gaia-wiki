<!-- TITLE: Staff Of Dionysus -->
<!-- SUBTITLE: A bull-skull rod -->

# Staff of Dionysus
* 1d8 bludgeoning
* Requires Attunement
* Legendary artifact
* +1 spellcasting

## Dionysus' Shield
 * When struck by a melee attack, the wielder can use their reaction and a 1st level spell slot to cast Dionysus' shield. This does not count as a spell cast per turn.
 * Dionysus' shield: Pushes the attacker back 15 ft. Deal (cha) force damage to them or none if the target succeeds on a strength saving throw.

## Dionysus' Horns
 * Whenever dealing force spell damage, the wielder can forego doing damage and instead move the target (or targets in the area if AoE) by (X) feet where X is the force damage from the spell. If the target hits an object or wall, they will take bludgeoning damage equal to the remaining feet in the path.

## Dionysus' Will
 * Allows the wielder to cast minor telekinesis at will. 
 * Minor Telekinesis: Range 30 ft. Manipulate and levitate an object weighing less than 50 pounds. You can exert fine control of the object. See similar restrictions on telekinesis for limitations.
