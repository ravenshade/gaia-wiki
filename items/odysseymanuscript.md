<!-- TITLE: Odyssey Manuscript -->
<!-- SUBTITLE: The original copy of Odysseus -->

# The Odyssey Manuscript
* Gives advantage on naval history checks

# History
* Contains information about what happened to [Odysseus](/chars/odysseus).