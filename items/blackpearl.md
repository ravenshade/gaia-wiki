<!-- TITLE: Black Pearl -->
<!-- SUBTITLE: A cursed item found deep in the sea. Made by Poseidon -->

# Black Pearl
* Shiny iridescent orb with blackish/gray gleams.
* Can be used once per hour.
* Causes 4d6 necrotic damage to target (except undead)
* Deals 2d6 necrotic damage to the user.
* Causes user to appear skeletal in moonlight.