<!-- TITLE: Reaper's Sickle -->
<!-- SUBTITLE: A short one-hand grain harvester of souls -->

# Reaper's Sickle
* 1d4 slashing + 1d4 cold one-hand sickle
* Can hold a single soul inside it