<!-- TITLE: Reaper's Scythe -->
<!-- SUBTITLE: A long soul-slashing scythe of true punishment -->

# Reaper's Scythe
* Two-handed heavy or versatile one-handed
* Two-handed Heavy 1d12 Slashing
* One-handed Heavy 1d8 Slashing
* Reach (+5 ft)

# Untapped Power
* Power requires the Honor of [Hades](/gods/hades) to imbue it