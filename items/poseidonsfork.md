<!-- TITLE: Poseidon's Fork -->
<!-- SUBTITLE: A legendary trident given to the Gelatin Kingdom. -->

# Poseidon's Fork
* d12 piercing + 2d6 water

# History
* Given to the [Gelatin Kingdom Royalty](/orgs/gelatin_kingdom) as a gift for their conquering of the island long ago.