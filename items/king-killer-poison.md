<!-- TITLE: King Killer Poison -->
<!-- SUBTITLE: A quick-acting lethal potion that incinerates the corpse -->

# King Killer Poison
 * A potion that acts within 6 to 12 seconds based on the target's constitution
 * The poison will incinerate the body of the target.
 * The poison can be ingested, injected, or put onto a blade.
 * The ashes from the corpse cannot be resurrected through normal means.

# Brewing
 * Requires a dragon claw from a chromatic dragon
 * TODO