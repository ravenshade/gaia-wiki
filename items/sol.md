<!-- TITLE: Sol -->
<!-- SUBTITLE: Sol the Bringer of the Sun -->

# Sol
 * A gift from Helios to Apollo during his duel against Zeus
 * Fell to Earth as the first falling star
 * The Titan Helios is trapped within the blade.
 * From the power of [helios](/gods/helios), it emits radiant sunlight.
 * The blade (with Helios inside) can damage gods.
 * Can be attuned instantly

# Stats
As the weapon is used, the power binding Helios to it fades and his power begins to be unleashed.

 * Returns to wielder's hand

 * Blade starts as a small sliver the size of a bolt or throwing knife. 1d4 fire.
 * 1d4 radiant. Emits sunlight.
 * 1d6 radiant. Dagger sized.
 * 1d8 radiant. Longsword sized.
 * 1d10 radiant. Staff sized.
 * 1d12 radiant. Greatsword sized.
 * 1d20 radiant. Morphing constantly. Burns wielder on nat 1.
 * 1d20 radiant. Massive ball of fire, like a small sun. Ranged, 120 ft. 
 * 1d100 radiant. User is engulfed in the fire like a fire elemental.
 * Sol is unleashed, Helios escapes. The blade loses its power. 
# History
 * When Apollo was young and wanted to prove his strength by rebelling against Zeus, the titan Helios forged him a weapon named Sol.

 * Apollo, with his hubris, lost the battle by losing the weapon. As the weapon fell into earth, it became the first falling star.

 * When the weapon landed, it destroyed swath of Morea. Poseidon, who wanted Zeus and Selene to rule the sky (since they favored him), decided
to lock up the titan for conspiring against them. 

 * Poseidon trapped Helios inside the blade that fell from the sky. 

 * Master Duplicamos intends to unleash Helios, who desires revenge against Poseidon and Zeus. 

 * The blade is kept secret and protected in the [lands of gibraltar, in the city of Fir](/places/delia/straits-of-gibraltar)
