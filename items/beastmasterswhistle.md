<!-- TITLE: Beastmaster's Whistle -->
<!-- SUBTITLE: The Whistle of Krag the Beastmaster -->

# Beastmaster's Whistle
* Ranger-only
* Charges: (wis-mod) per day.
* Requires bonus action.
* Causes your beast to go into a [barbarous rage](http://5etools.ravenshade.com/classes.html#barbarian_phb,sub:barbarian,f:rage%201).