<!-- TITLE: Krazzaks Orb -->
<!-- SUBTITLE: An orb that krazzak is trapped in -->

# Krazzak's Orb
* Warlock or Wizard
* Gives the ability to have a changeable imp
* controlled by [Krazzak](/chars/krazzak)
* Cannot violate terms: He wants revenge on Hades, can't make him go against it.

# Orb of Control
* Krazzak's orb is an orb of control
* Requires a strong sacrifice to free him.
* Sacrifice equal to *probably* all the imps he had (couple dozen, ~40)

# Spell Storing
* Krazzak, as a warlock, can hold 1 spell up to 5th level for as long as needed.
* Expending the spell slot requires a siphon equal to (spell lvl / 2) lives.
# Siphon
* The orb can cast siphon at will with an action. Siphon sucks the life out of a being
* The orb can hold up to 3 lives of siphoned blood outside of it. The size of the creature affects the amount. 1 "life" is approximately a human size.
* The siphon draws 10 points away from their maximum hitpoints. 
* Melee range.
* CR lines up with value of 'lives'
