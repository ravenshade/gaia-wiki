<!-- TITLE: Elven Bow -->
<!-- SUBTITLE: The famous Bows of Artemis -->

# Elven Bow
* +1 bow
* +1 dmg, +1 to hit d8
* Cost: 400g