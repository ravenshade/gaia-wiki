<!-- TITLE: Control Amulet -->
<!-- SUBTITLE: Amulet of Control -->

# Amulet of Control
 * Created by Daedalus for Ariadne, has his sigil on it
 * Made out of iridium
 * Wearer reduces all spell slots by 1