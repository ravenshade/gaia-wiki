<!-- TITLE: Last Word -->
<!-- SUBTITLE: A sword that cuts with words -->

# Last Word
* Deals psychic damage instead of physical
* Does not leave a wound, but those hit cannot speak for a long duration.
* Created by [Athena](/gods/athena) for individuals who express debate prowess.