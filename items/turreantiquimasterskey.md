<!-- TITLE: Turre Antiqui Master's Key -->
<!-- SUBTITLE: An ancient key that was born with the tower. -->

# Master's Key
* Allows transportation between The Master and Apprentice's tower.
* Transportation only happens laterally.