<!-- TITLE: Silverstein Crown -->
<!-- SUBTITLE: A jeweled crown of Queen Silverstein -->

# Silverstein Crown
* Allows user to manipulate as much water as they have levels in gallons.
* Magic spellslots can be used to multiply this amount.

# History
* Created by Poseidon
* Lost to the world, in the hands of the [Despotate of Morea](/orgs/despotate_of_morea)