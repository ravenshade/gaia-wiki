<!-- TITLE: The Silencer -->
<!-- SUBTITLE: A bow that makes the arrows it fires invisible. -->

# The Silencer
* Any projectile shot with the bow becomes almost perfectly invisible.
* A perception check over 20 can see the arrow.
* The arrow's invisibility lasts for 24 hours.
* The thread of the bow is invisible. 
* Arrows coming out do not make noise on impact.