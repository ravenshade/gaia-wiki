<!-- TITLE: Capacitor Gloves -->
<!-- SUBTITLE: Lightning Striking Gloves -->

# Capacitor Gloves
 * +1 to hit
 * Requires attunement 
 * Every unarmed strike (sun bolt included) generates 1 charge.
 * Every 10 charges can be used to cast Lightning Bolt at level 3.
 * Charges do not continue to accumulate while at full capacity (10). 