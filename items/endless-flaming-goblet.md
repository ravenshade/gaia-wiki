<!-- TITLE: Endless Flaming Goblet -->
<!-- SUBTITLE: A gift from Hephaestus -->

# Endless Flaming Goblet
 * Hephaestus' gift to appease Dionysus during the Knossos conversion to him. The goblet is full of alcohol and is constantly burning. The fires are warm to the touch, but will not burn.
 * Requires attunement
 * Resistance to cold damage.
 * Provides 15 feet of bright light and 15 feet of dim light.
 * Contains Firewhiskey, a cinnamon-spiced whiskey. The whiskey refills linearly over a period of 12 hours.