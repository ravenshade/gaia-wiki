<!-- TITLE: Cerberus' Fang -->
<!-- SUBTITLE: A powerful fang -->

# Cerberus' Fang
- d4 slashing + d4 necrotic
- (lvl1) roll a d20, on a 3, do d4 fire
- (lvl2) always d6 fire
- (lv3) 

# History
* Extracted from the noble beast when [Hercules](/demigods/hercules) defeated it.