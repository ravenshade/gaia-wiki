<!-- TITLE: Clockwork Staff -->
<!-- SUBTITLE: A staff that uses time magic -->

# Clockwork Staff
* Requires attunement. +1 spell attack.
* Has a clock that usually has the correct time.
* Has UTC (Universal Time Connection) interface on bottom.