<!-- TITLE: Staff of Withering -->
<!-- SUBTITLE: A quick summary of Staff Of Withering -->

# Daru's Staff of Withering
* [5etools Link](http://5etools.ravenshade.com/items.html#staff%20of%20withering_dmg)
* The staff is currently in a deactivated state. If you place Krazzak's orb within its thorny grasp, it will gain its power.
* This CAN be used while shapeshifted while attuned because of Daru's special relationship to it
* This staff is at a crossroads as an artifact. If your actions help restore nature back to balance, it will grow into a Staff of the Woodlands. If your actions spread pestilence and harm nature, it will wither further into a darker staff of Hecate. 

# History
The corrupted thorn thicket that you have dwelled in was originally a druids grove you made. Part of the thicket has grown into a magical artifact in the year that you have been treating the swamp. One day, you broke off a large branch of thorns, and you wield it as your staff. If only it had a magical orb that could be used as its power source...