<!-- TITLE: Selene's Grace -->
<!-- SUBTITLE: A braclet that has a shimmering moon floating around it -->

# Selene's Grace
A bracelet made of hardened silver. When worn, a shimmering moon that matches the current phase appears to orbit the bracelet.

After it has been used, the moon disappears.

Rare. Druid-only. Requires attunement. 

The bracelet allows the wielder to transform a willing creature into a beast of CR 1/8 or lower. The creature is shapeshifted similar to a druid, maintaining its intelligent stats but not its physical. 

The creature will stay shapeshifted until they expend a bonus action to unshift or until a time duration past when the caster would unshift if this were their form.

After it has been used, it cannot be used again until midnight. 