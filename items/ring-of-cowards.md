<!-- TITLE: Ring Of Cowards -->
<!-- SUBTITLE: A ring fit for a coward -->

# Ring of Cowards
A ring with a signet hailing the Kor Dynasty dwarven insignia. This ring was famously used by the king with his bodyguards against invaders.

Rare, Ring, +2 AC normally, requires attunement

The ring has 6 charges and regains 1d4 charges at dawn. If the last charge on the ring is used, roll a d20, and on a 1 the ring becomes non-magical.

Charges can be expended as a reaction for the following effects. Only works on melee, ranged, or spell attack rolls:

 * 3 charges: Inflicted damage is divided in half between the attuned wielder and an object not worn or carried within eye sight.
 * 6 charges: Inflicted damage is reflected back at the attacker.