<!-- TITLE: Summary -->
<!-- SUBTITLE: Events from the party -->

## Sol: The Blade that Kills Gods
Late Autumn, 218

 * Party woke up in the Temple of the Moon while monks were burying dead.
 * Party noticed the water was particularly calm. 
 * Party assisted in burying the dead monks, Emmett learned of Flak's anger
 * Party followed Flak to the river where they learned of Sol, a legendary artifact
 * Party went back to Temple to get something to show Firbolgs who they are
 * Party, when heading back, ran into Duplicamos (master disguised apprentice)
 * "Apprentice" told them about (Plans), urged them to protect Fir.
 * Party went to Fir, saw the cultists, and immediately slaughtered them.
 * Emmett realized something strange when looting bodies, noticed the fur.
 * Party tried to convince Firbolgs they didn't know about the spell, firbolgs confused.
 * Lucius used magic to talk despite language, they were going ot back down
 * Emmett/Barnabus snuck down the chimeny and killed one of them, other ran.
 * After making through traps, party went to door in a protected area.
 * Lucius attempted persuasion, but had a volley of crossbows unleashed.
 * Hot oil was also poured down to kill Lucius.
 * Lucius teleported away, Emmett dropped a bomb down into the oil.
 * The bomb destroyed their fortifications and exposed the tunnels.
 * Party killed firbolgs, scouted with Spider Imp, found the Shaman Firbolg.
 * Party used giant badgers to dig to Sol's cage after scouting.
 * Shaman summoned Storm, Wind, Fire, (and Water) against the party.
 * Party got Sol, which was blinding bright, and escaped with their lives.
 * Pina got Sol to the d6 level
 * The elementals fought each other after the concentration was broken.
 * Emmett dodged the fire elemental after entire room was smoking.
 * Party rested in the old growth oak forest.
 * The Master (real) approached them and told them of the Apprentice's treachery
 * The Master tells them that he needs them to assist him to stop his apprentice
 * Lucius received a lot of burns and healed with Necrotic
 * Lucius communed with Poseidon and heard "Never trust mages. I am currently siding with The Master"
 * The Party level up to 10.

### Meanwhile
 * Master Duplicamos finishes his preparations to begin the ritual to unleash Charbydis and Scylla.
 * Master Duplicamos is arranging transportation to take the Apprentice into the Underworld.

## An Unexpected Storm
Mid Autumn, 218

 * Ariadne received multiple sendings from King Asterion
 * Ariadne leaves because Knossos is facing terrorist attacks. Leaves lute. **(ARIADNE LEAVES PARTY)**
 * A bard named Barnabus Bluard showed up, angry that he dismantled the polymorph ring **(BARNABUS JOINS PARTY)**
 * Barnabus hails from Apol and has visited the strange lands of Arcadia before.
 * The party accepts him because he wants to continue 'their elaborate prank'
 * The party investigates the shrine of selene and finds the missing staff
 * The party goes down and finds the beautiful Temple of the Stars.
 * A mushroom-pianta Lucotus greeted them and showed them a tour
 * Lucotus told Emmett of a spell that was cast to make the pillars indestructable.
 * Lucotus told Emmett that learning how it was removed... Athena's library in Arcadia
 * Lucotus explained the trouble of cultists they expect the temple to have
 * The party sets off against the coast, builds traps, tripwires, and waits.
 * The party is assailed by 5 boats with cannons, hordes of cultists.
 * Quadarius fights with the other monks, barely could not fight them off.
 * The party is hit by a tsunami, destroying the forest and parts of temple.
 * The party defeats one of duplicamos' images, the other survives.
 * The other duplicamos image managed to acquire the star of selene and escaped.
 * The cultists ran, but were hunted down and killed.
 * The temple entrances were mostly destroyed, but the inside is impeccable
 * There are hundreds of corpses of cultists (Atlanti), bodies, etc.
 * Around half of the monk order died, including Mushrus the old man.

### Meanwhile
 * Master Duplicamos has discovered his Apprentice has been working against him (because of the sending stone used in combat)
 * Master Duplicamos has the full Staff of Selene and needs to perform the ritual. 
 * Master Duplicamos plans to try to sabotage the patriots by tricking them into attacking the furbolgs and stealing their relic using Mirage Arcana and Seeming
 * Master Duplicamos has captured the apprentice, stolen his belongings, and locked him up in Turre Antiqui - later to be sent to the Underworld as punishment.

### Plans
 * The loud ocean by the temple no longer roars. It has gone quiet, the ocean looks still. (due to Star of Selene)
 * Master Monk will give a letter to Pina if he sees more night piantas asking for aid.
 * The monks will begin burying their dead. A young pianta will be seen burying his friend, who is later arguing with Lucotus. (wants to tell party about Sol).
 * Sol is a weapon guarded by the Firbolgs, hidden away in the temple to avoid people using it.
 * Duplicamos (master) will meet the players, pretending to be apprentice (shows stone, explains it has been revealed). 
 * Duplicamos (master) will tell them the city of Fir was attacked by cultists. They are searching for a legendary artifact the firbolgs protected. (lie)
 * Duplicamos (master) casted an illusion on Fir to make them appear to be cultists, and sent contingents of cultists to die and kill firbolgs (but only a few)
 * If players go to Fir, Master will appear AS Master. He will tell them that his apprentice is dangerous, a false story of his birth, and that he has no intention of saving pillars.
 * Master reveals illusion, show how the 'apprentice' was manipulating them. Explains false story about Char/Scylla and how he was simply moving them, Duplicamos wants to use them.
 * The master will leave, and tell them that they need to empower Sol, and use it against his Apprentice.

## The Price of a Soul

Mid Autumn, 218

 * The party ran around the Kor mountain, made a map, found the hydra.
 * The party killed James Bonnett (boomerang), spared Theed.
 * Theed and Kiwi escaped, ran out the door to find Carrie.
 * The party closed the door and left. Party headed to Kharanos.
 * The party found Carrie. Carrie was close to James, had diamond.
 * The party killed Carrie as she tried to escape and suggestion them.
 * Carrie's body was left in the swamp in a puddle. Diamond was taken.
 * Party went to Krag'nush, the charred desocrated ruins, to hunt a reaper.
 * The party attempted to revive Lucius but failed to. He was not willing.
 * Lucius was sent to the underworld, but his body was not buried. He began the walk.
 * Party contacted duplicamos and asked for help, he mentioned Lord Azul.
 * Nop drank so much that he became Ariadne.
 * Party put all the orcs in a single pile and hid until the reaper came.
 * A reaper came, ice following them. Reaper stole an orc's sole with sickle.
 * Quadarius offered his Amulet of Feign Death, Ariadne offered Nop's soul.
 * The reaper gave them visions of what he would do, and that he would accept.
 * The reaper took Nop's soul to replace Lucius', and the amulet to let him stay.
 * Lucius is now considered undead. Krazzak was given back to him.
 * The party traveled out of the swamp, Daru stayed to heal the swamp.
 * Party traveled across the mountains, saw the Firbolgs, kept going.
 * Party reached the Shrine of Selene (crescent moon, marble, covered in moss).

### Meanwhile
 * Delian army and Spartans sacked Artem. Burned the entire tree down, exposing the pillars. Knossos tried to stop this, but the goblin weaponry (from Colchis) and Spartans were ruthless.
 * King of Artem was able to escape and is in hiding. The Queen of Artem had disappeared several days before.
 * Knossos, due to its inability to produce weapons with its manufacturing missing, is starting to have money issues. 
 * Colchis has employed its goblins backed by goliath labor to produce large quantities of weapons for Delia.
 * A small band of Morean soldiers (War Dogs freed) have recently appeared and causing acts of Terror around Knossos (War dogs, Jeremy, daughter, rebels, living forest druids). 

## The Fall of the Black Dragon Horme

Early Autumn, 218

 * The party contacts Duplicamos to ask for a Diamond, he recommends druid or dragon.
 * The party long rests in a hut (9 days raise dead).
 * The party heads to the Daru Thicket, they met him and a group of turtles.
 * Daru explains his position, says he needs to heal forest. Vines all over him.
 * Nop suggested Daru leave, this assisted Daru a lot. The party formed.
 * Emmett recalled that the king has a diamond in his crown
 * The party started to head to Kor mountain, but stopped at Krag'nush.
 * Krag'nush had been incinerated and smoke was everywhere
 * The party ventured in and struggled with suffocation, but managed to escape
 * Emmett found the Amulet of Horme and equipped it immediately.
 * The party moved towards the ritual circle and found orcs in a ceremony
 * The party let them finish and then assassinated them without taking damage
 * The party hid themselves -- Pina in a tree, rest in the hut.
 * Horme flew down and searched, he barely found Pina. Pina outran him.
 * While Pina was running back, the others were able to rest until 4am.
 * During the cover of night, the party built a contraption
 * The contraption had a set of gems & dwarven blunderbuss to lure him
 * The dragon knew it was a trap, but wanted the blunderbuss.
 * The party barely was able to defeat the dragon, he flew away.
 * Quadarius hit the final strike, but the dragon landed on the summit.
 * The deathwarden healed Hormes, but the party flew up to the summit.
 * Emmett ran up the mountain and used the necklace to open the door.
 * Emmett ran into the library and found James Bonnett stealing the crown
 * After a quick duel, Emmett was able to defeat Bonnett and stop him.
 * The rest of the party killed the deathwarden and Hormes, barely.
 * Daru felt cured of his ailment and the party captured Theed & Bonnett.
 * Party gains [Hormes Hoard](/notes/hormes-hoard)

### Meanwhile


## The Slaughter of Krag'nush

Early Autumn, 218

 * Party headed back to town to talk with Adrian about his party.
 * Bet on the tab for Adrian. Adrian offered party his 2-hand sword.
 * Read about dragons, learned of culture. Woke up around sunrise. 
 * Party got across the rickety bridge and traversed the swamp.
 * The party found Billy Bob's backpack and frying pan.
 * Ventured deeper and found Krag'nush, heading to Ol' Kharanos.
 * The party attacked Krag'nush from a distance, Chieftain & warriors attacked.
 * The party used the Kingkiller poison on Chieftain, he died and melted.
 * Emmett snuck around and helped James Bonnett escape. They got out.
 * The party reunited by Daru Thicket, James Bonnett offered help, they refused.
 * James Bonnett ran off to the rickety bridge. Party advanced to Krag again.
 * Party attacked Krag'nush when Phyrus and Yotul were there. 
 * The party threw two fire elementals, Emmett snuck behind.
 * The party was victorious, but Lucius Creed died to an orc war chief.
 * One uncontrolled fire elemental rampages through Krag'nush.
 * The party fleed to the swampy forests nearby with Leucis' corpse. 

### Meanwhile
 * Orcs begin to run to the mountain after hearing of the attack for safety.
 * The orcs barricaded the entrance and have set up an ambush.
 * James Bonnett started heading to the mountains to try to steal the dragon hoard.

## A New Land
Early Autumn, 218

 * Emmett puts the cannon on the front. Attaches wheels to the cannon.
 * Lucius reads the whole 2 weeks. Pina was lookout, also read Haunting.
 * Quadarius and emmett dueled. Quadarius won.
 * Emmett spent the rest of the time making bombs, learned new recipe.
 * Quad buys 100g weapons -- seagull (spy network + buy/sell)
 * A Delian League ship stopped and inspected them by Phillip Longley.
 * Nop played a trick on a guard and cleric, gained Dolos favor.
 * Inspection was passed. They continued through a storm (boomerang).
 * The party arrives at North Snaghead (6pm).
 * Emmett meets Slim Shady (2 goliath, 1 mastiff) and buys cheese (2s).
 * The party spoke to Sees No Water & an old man goblin.
 * Lucius, Quad, and Ariadne go to Snail Trail Inn, Tortle Innkeeper.
 * Tortle Innkeeper tells them about a map merchant & the orc problem.
 * Emmett & Pina visit the Lighthouse, meet Half-Orc named Kraggle.
 * Emmett attempts to put a bomb down, Kraggle stops him.
 * Lucius, Quad, Ariadne visit map merchant (Thunk) and learn about territory.
 * Lucius, Quad, Ariadne went to the Shellrock Inn. Saw goliath slaves.
 * Lucius asked slave for freedom. Said no. Found another one for freedom.
 * Party bought inn stay (2g), heard about disappearances in the Inn.
 * Party assassinated goblin, prestied, stored corpse. Freed goliath.
 * Party claimed that it was a disappearance, innkeeper believed.
 * Emmett and Quad went back to boat, saw Sees No Water (who pickpocketed).
 * Quadarius killed Sees No Water. Emmett finished him. Left beheaded body.
 * In the morning, the party checked up on all the inn residents.
 * Party met Adrian Gorgolloth, learned about dragon and pillar in Kor mountain. Adrian was wounded by it, his whole party died.
 * Emmett walked out, saw tabaxi fishing corpse, Emmett told them.
 * Fight broke out, Quadarius saved some tabaxi, they ran away.
 * Party rejoined, resolved matter by paying retribution, paid goliath 1 gold/day.
 * Party left glitterbug and "Greywurm" goliath guarding the boat.
 * Party walked by caravan selling exotic animals. Ariadne saw wolf, attacked.
 * The party assassinated the goblin merchant. The animals turned out to be polymorphed citizens.

### Meanwhile
 * Spartans officially joined in the war on the side of the Delian League. 
 * Knossos is winning the war against Artem, their territory now extends near the Temple of Artemis. Sieging the city. King Leonidas leads the charge.
 * General Aegeus, enraged about Theseus' passing, has gone mad. 
 * Vernon Alabaster, who escaped from the Labyrinth when the heroes did, and a contingent of Morean soldiers have escaped near Knossos.

## A Traditional Knossian Banquet
Late Summer, 218

* Party used Disguise Self to give themselves nice clothes.
* Party went to Banquet. Party was knighted for Knossos by Ariadne-Lydia.
* Quadarius upset Swindley and stole his feather cap.
* Sacrifice to Dionysus made part of the audience upset.
* Emmett stole from drunken nobles successfully.
* End of the evening, assassins showed up. Pina caught one of them.
* All sons of Barleybrews were killed. Duchess Anna Henrietta was killed.
* Duplicamos was mazed by the Candora Aristocracy
* Party asked Asterion to make antivenom. He was able to create one.
* Lydia was saved. Kingkiller poison was used as antivenom. Lydia's mother was upset.
* Leonidas took prisoner to Guard Barracks and interrogated them viciously.
* Party went back to boat, saw riots and stopped them from killing woodelves.
* Kreos was drunk at boat. Had just fought assassins.
* Party slept. Went to prison, saw Leonidas viciously attacking.
* Party offered to kill prisoner in exchange for info. He gave info.
* Theseus knew about his son's death, blames Ariadne and Patriots.
* Quadarius killed the prisoner.
* Ariadne tracked down the beastmaster in the slums. Commanded to stop wolves and to use peasants/prisoners instead.
* Pina found a merchant shuffling potions. Bought crates. Told about Shrine Sprites from Delfino.
* Emmett stole a cannon using charm person. Guards informed later.
* Kreig made cannons for Emmett and gave him (24). Crown created for king is finished.
* Kreos stayed for his battle.
* The party set sail for the Straits of Gibraltar.

### Meanwhile
 * General Aegeus learned Ariadne is back, alive, without Theseus. 
 * Billy Bob is being searched for.
 * Duplicamos talked with Sabrina. Duplicamos argued why he needed Patriots not involved with Knossos anymore. Needed Artem at war (for the bodies for Hades).
 * Sabrina told him that was directly against Delian interests.
 * War between Knossos/Artem resumed. 'Master' blacksmith left to front.

## A Grand Gift and Many Paths Ahead
Late Summer, 218 

 * Party discussed strategy. Exited the maze. Ariadne explained & introduced Minotaur.
 * Crowd gaped. Tentative, but trust Ariadne. Knights escorted back to town, lockdown.
 * General Rockfurt learned that Daedalus is dead, Theseus is dead, Minotaur is the heir.
 * General Rockfurt got deed for manner. Dressed Minotaur like King. 
 * Party saw Minos' old room. Found stuffed white bull. Found sending stone. Room is massive, full of vaults, massive bed and crystal chandelier
 * Party went to see Manor. Found contractor gnome Swindley. Asked details. Knossos paying for manor.
 * King Leonidas & Duplicamos arrived. Dinner arranged, postponed to next day.
 * Ariadne organized Kreos arena fight for Leonidas. It was rough for Kreos.
 * Duplicamos informed cult forming around the Vortex. 
 * PR session where Ariadne addressed group. Informed Patras as heroes who broke Labyrinth.
 * PR session had Thea's father about war, Noble about God support, and Duplicamos about loyalty.
 * Party met with Asterion, who is eating sheep. Arranged construction for tunnels.
 * Party decided needed a shadow king and puppet. Informed General. He accepted.
 * Lucius tried to negotiate away a 20,000 gold fee. Asterion worried about choice. Ariadne was sassy. Treasurer is suspicious.
 * Party went to sleep on boat. Informed Kreos that Nop is dead, Kreos was happy. He drank some fine wine.
 * During the night, Lucius had a dream asking him to help control Charybdis at the vortex.
 * In the morning, party went shopping. Ariadne slept in chambers. Met Lydia at The Heady Topper.
 * Lydia and Ariadne bickered, but decided that Lydia would pretend to be Ariadne and transition in the Minotaur. Made a lot of bull jokes.
 * Quad & Emmett investigated Daedalus' tower, found a lot of sigils. Sigils use electricity to work.
 * Pina went to Corona, heard brewery doing well.
 * Nop went to bar. Got drunk. Lydia found him and raged at him for giving away.
 * Lydia was coronated as disguised. Met Minotaur, got along well.
 * Party prepared to head to the scheduled dinner with the royalty.

### Meanwhile
 * The party was aged 6 years by the portal.
 * The labyrinth exterior is an abandoned fake version of the maze.
 * News of Androgeus' death has spread.
 * Rockfurt issued a lockdown because Daedalus is missing. 
 * Leonidas watched Kreos fight a bunch of wolves. A LOT of wolves.


## Daedalus' Exit to Mechanus

Late Summer, 218

 * Wake up. Head to mountain. Find Grak dead by the side. Krazzak singed.
 * Patriots put gnomes next to Grak. Grak saved by Nop.
 * Party got past Grak. Grak let them pass if bring back Ariadne.
 * Party got past bridge by using Ring of Jumping. Party got to caldera.
 * In Caldera, found sigil that led to flame stone and info on fixing.
 * Party followed blood trail to forest, saw children, talked with Tbone squirrel.
 * Party found Vernon, badly injured but with stones. 
 * Party went to stone gate. Saw Ariadne. Tried to steal items. Failed.
 * Party went into maze. Long rested. Used strat to go through straight.
 * Party reached gate. Put in stones. Did nothing, heard slight sound.
 * Emmett dived under hedge, found void bridge. Party joins them. Ariadne follows.
 * Party experiments with watches. Found out haste/doubling mechanics.
 * Party walks into hall, sees ton of gnomes and mechanics. Interrogates one.
 * Party heads down to the low levels. Walks through labs.
 * Party finds grand library, easily finds several books and the wonderous one
 * Party plays dice poker, Nop wins the book.
 * Party meets Asterion. Asks questions. Drinks from gauntlet.
 * Party convinces/suggestions Asterion to join them and escape.
 * Party heads to lowest level. Run into Haskell projection. 
 * Party fights the clockwork dragon with Asterion. '
 * Nop exchanges dolos favor, gets swapped bodies with Ariadne. Nop dies.
 * Party finds high office documents, flickerhouse documents. 
 * Party finds trinkets/gadgets. Throws Nop into void. Throws Theseus into void.
 * Party goes up to the lab, finds journal, goes up to Daedalus' Quarters.
 * Party uses the sigil to summon Daedalus. He tells them they had won and need to leave, now.
 * Daedalus and Asterion part. They loot a vault and get teleported back.
 * Daedalus apologies about the age. Get aged a significant amount for the split.
 * Daedalus launches the labyrinth into the Mechanus plane.
 * [Items haul](/notes/labyrinth-items)

### Meanwhile
 * Flickerhouse industries is completely abandoned. Miles Flickerhouse is missing.
 * Jeremy is revived as a dragonborn by his daughter. 

## The Graveyard of the Daft
 * The group looted the piles of gnome corpses and clockworks.
 * The group handed the corrupted basilisk key to Vernon Alabaster.
 * Vernon set off to retrieve the city stone. The party set to Graveyard.
 * The party passes by the The Lost Ones, enters the Graveyard of the Daft.
 * In the Graveyard, they find symbols, dig up corpses. Find Animated Armor. Named Glitterbug.
 * Luc performs ritual, brings back Animated Armor. Group goes to Haunted Manor.
 * Party runs into Ariadne, asks for help for Theseus. They descend in.
 * The group explores the manor. Run into Rocking Chair ghost, scare it off.
 * Luc finds a book to move out. Fireplace passage appears, crawls through.
 * Emmett finds gnomish wind generator. Finds journal describing manor history.
 * History: Construction never finished. Too many ghosts. Killed humans/gnomes.
 * Explores the tunnel, finds a barricaded section. Destroys it.
 * Finds a wind fan controlled by ghost. They destroy the wind fan.
 * Party goes back, goes through dining room & kitchen following blood trail.
 * Enters basement. Fought Murdered, Beheaded, and Diseased the three-ghost trio.
 * Party defeats trio. Tracks them down and kills them. Destroys their hearts.
 * Party destroys the furnace and the smoke above abates. Ghosts escape.
 * Party destroys mimic. Find a small loot horde - charlatans, billowing, hat of vermin.
 * Party leaves, makes a Tiny Hut. Party begins to rest and catches up with Theseus.
 * Ariadne pickpockets the ghost key out of the Haversack while Theseus talks.
 * Emmett pickpockets Theseus' diary and gold, find out Theseus killed Androgeus in a duel.
 * Quadarius shoots Theseus as he sleeps, crits twice. He's permadead.
 * Ariadne geas Emmett to stop Quadarius. Grabs Theseus. Dimension Doors out.
 * Quadarius argues that Ariadne is using Theseus to become the heir.

## The War Dogs
 * Party looted basilisks. Got one-time use fang. Got scales.
 * Party went to Dolos Statue, sacrificed tiefling (now blind) with trick.
 * Nop convinced tiefling to roll in fountain. Got a favor from Dolos can use anywhere.
 * Party backtracked to signpost. Wandered past mountain to deep forest.
 * Party was ambushed by war dogs. Fought them off. Got a warning letter.
 * Party used white flag and insisted War dogs listen to them. Vernon allowed.
 * Vernon mentioned only reason why is because guardians are coming.
 * Vernon said if they assisted with defending against guardians, would help.
 * Vernon brought old man Brash who once got all the stones & keys.
 * Group prepared. Massive tank, constructs, and gnomes assaulted the fort.
 * The fort was almost entirely destroyed. The puzzle room was exploded on.

## Into the Labyrinth
 * Party heads to the labyrinth. Cheer on outside about diving inside.
 * The party meets Haskell, Haskell gives them a garden key and timepieces.
 * The party goes directly towards the mountains. Uses the large creek.
 * The party reaches the mountain, meets Troll named Grak. Grak ate a human.
 * Party requests Grak move rocks, Grak throws rocks to stuff water.
 * The water level lowers, stuffs, stops the rotation. Guardians let it open.
 * Party travels in empty creek until towards the middle. Water begins to run.
 * The group finds the sewer vent. Emmett undoes the plate, throws it in.
 * Guardians and repair team track it down and begin repairing.
 * Emmett is lowered down via rope, kills two repairmen. Hit by a guardian.
 * Party pulls back, he escapes without gears churning him up. Escape from watcher
 * Party goes to Garden. Meets Otis & crew, quickly solve the plant puzzle with sprite.
 * The group enters. Grabs the chest that emits light when unlocked.
 * The party uses ropes to get up the top. Found the Plant-Stone.
 * After finding the plant stone, using the light, got through the venom gardens.
 * Group used darkness and a sheet of dishes and tailor's potions to fight.
 * Fought the basilisk. Basilisk is dead with fangs down. Tears cured Emmett.


## The Bull-headed King's Secret
Late Summer, Year 218
* Party split - Nop & Pina in Knossos, Emmett & General by Ur, Luc & Quad far
* Nop stayed with Lydia in her manor, learned about Ariadne running off
* Pina did some shopping and learned about the Queen's sudden death
* Nop and Pina met with Kreos in the arena, Kreos fighting Nautilus
* Nop makes a 1000g bet on Nautilus against Kreos, bribes 500g for entrance
* Nop and Pina attend the arena battle, Nautilus wins though it is close.
* Nop pays the 500g and they visit Kreos. He will need a couple day recovery.
* Lucius and Quadarius fashion a sail, found by Rockfurt's rescue.
* They inform Rockfurt of the King's body's location, Rockfurt sails for it.
* Emmett w/ General to mountains, find soldiers, he convinces about peace.
* Emmett goes to Fates (see predictions), then travels to Knossos.
* Nop & Lydia have a great time, Pina goes purchasing some basic items.
* Emmett purchases some tinkering items, all of them eventually meet at boat.
* Rockfurt approaches boat (after it is towed), tells need to interrogate.
* Party travels to Castle, Rockfurt informs about the will found on Queen
* **Will:** Queen leader, lock up Daedalus, reward Patras for help.
* Party agrees to track down the two heirs (one missing, one in bull run)
* Party goes with Adonis, lets into Daedalus' tower, meet with him.
* Daedalus reveals the bull story of the past and the secret true heir.
* Dadaelus also revealed secrets, but wants to see how they will do in the maze

### Meanwhile
* Ariadne and Theseus went to bull run. He challenged Androgeus, the other heir. Androgeus was killed.
* Amber is abducted by the agents of Poseidon.

## Parley with the Kings
Late Summer, Year 218
* Nop stayed with the elven lady for a day of fine romance.
* Seph got a notice **Gelatin under attack** and went to investigate.
* Kreos went to the fighting ring of Knossos.
* Pina decided to stay with the Coronas and assist with the lighthouse during the navy embark happening that day.
* Quad, Billy, Amber, Emmett, and Lucius scoped out Minos' schedule, decided to investigate his chambers.
* The party runs into the guard, learns that the king is very well liked.
* Emmett finds a deaddrop with a haversack on a wall outside the Knossian castle. Contained a dead parrot, cage, and some equipment.
* Party does a favor for the scribe, goes to Twindley's (a tailoring shop that is actually a soup and crab shop). Waits for King.
* Spoke with Minos, insisted that neither side is at fault, there is duplicity from a third party. 
* Party convinces Minos to meet with General at their normal safe spot, the Cave of Artemis.
* Minos employs an enslaved mage to teleport them to Artem. 
* The party talks to a major, communicates with general, then rides to the mountains (7 hours)
* The party, minos, and the general meet in the cave (which Minos had trapped). Agree to a two-way union against Morea & Delia through claiming the Morean Tip as their own.
* Emmett and Quadarius visit the fates. **Fates**: King of Knossos will die, Tower of Babel will be destroyed within an hour, Poseidon is deceiving Hades, Hades is deceiving Poseidon. King Minos will die. Emmett could be the one to kill Minos. Never will Knossos belong to the Moreans, Knossos will stand tall.
* The party rushes past the General's army with Minos, General orders them not to move until this matter has been resolved and he says so.
* The party contacted Pina & Blorge to turn out the Lighthouse to avoid the fleet from seeing the Morean forces.
* Billy bob goes up for supplies, group suspicious of General, general forced out, almost killed by mages' lightning bolts. 
* The group destroy most mages fireballing tower. Mages teleport to Minos at bottom. Minos deceives group, mages teleport away (4 survived).
* Minos uses a great fire hammer to destroy a massive chunk of the pillar of hercules. Pillar falls onto Midas.
* Status: Emmett with General riding off, Quadarius and Lucius on raft. Near midnight.
* **Party**: Billy Bob was washed off to a swamp where he set up a coconut shop.
* **Party**: Amber Fireforge was swallowed by the ocean and found herself in Atlantis. 

## The General's Request
Late Summer, Year 218
* The group reaches Artem, talks to the great tree. The tree responds with two voices and tells them about how long it has been there.
* Group went into Artem. An upset guard greeted them and informed them of their arranged meeting.
* Emmett went to the backwoods and met Vasso the black market dealer.
* Kreos went and gathered magical supplies from Quiver's Elixirs
* Billy Bob got arrows, crocs got relaxation, got spices
* Pina met three other piantas and had a fantastic lunch with them
* Amber went around information gathering and found a lot of information about the tree and history
* Others did some general purpose shopping.
* Quadarius went to check on his stock purchases, took the whole day.
* Afterwards, the group met with the General. The general revealed his true appearance. He told the group not to mess with the Fletcher Rebellion (explained it was his wife). He offered the group 30,000 gold for disabling the hierarchy of Knossos to disable the defense. He gave the group a trieme and the ring of protection (which was taken by force from Quiver). 
* The group set sail that morning. A hawk tailing them was noticed, they greeted that hawk. The hawk was Jeremy.
* Jeremy lied and said his daughter was going after the minotaur. They caught the lie eventually and interrogated him. He mentioned he was sent to spy. He threatened them as they continued to talk. He eventually left.
* Jeremy came back during the night, they caught him, and then fought him. The battle was close, he tried to escape, they captured him after barely knocking him out.
* Jeremy was brought aboard the boat and is unconscious. 4 days to reach Knossos.
* Quadarius woke up in a crate after losing stocks.
* Party killed Jeremy after he tried to escape. Sucked w/ Blood Orb
* Sailed for a couple days. Nop found Poppy. Befriended. 
* Billy bob made excellent soup for her. Party drama happened.
* Group told Poppy about dad and lucidius. She flew to revive him (10d). ** Required ** making a deal to end the Fletcher Rebellion for spices from Alleria.
* Group reached Knossos. Passed checks from Admiral Rockfurt
* Trieme docked on the 14th dock. 
* Emmett killed a guard (Krenal) took his identity and did triage
* Billy bob, Seph, Creos hung out at the boat
* Pina spent the night with his Corona family, as did Quad
* Amber, Lucius went to Cult of Dionysus -- debauchery, chicannery, etc
* Nop went to College of Glamour, everyone knew him, played amazing
* Nop spent the night with a elven lady

## The Rebellion
Late Summer, Year 218
* The group destroys a set of guards outside the Temple of Artemis
* The group sneaks into the Stronghold through a disguise with Morean tabards
* The group runs into the General and the Magister. They get paperwork registered as the navy.
* The group goes down into the dungeon and found 4 elves, Kreios, and Lucidius.
* They destroyed two jailors and freed the prisoners. Lucidious was made invisible and sent out to start a rebellion.
* The group fought through the first level of the Athenian Stronghold keep, but a soldier sent out notice about an invasion.
* The rebellion started right after all the guards outside began pounding on the door. No assistance was provided.
* The group set a massive explosion by the door of the keep. 
* The group cleared the second level of the Athenian Stronghold keep. Found boots of elven kind + gold + potions.
* The group hid in a Rope Trick (long rest), meanwhile the rebellion forces were destroyed. It is 12:30 AM.
* The group went up to the top, stealthily killed two archers. 
* The group killed [Magister Tassos](/chars/magister-tassos) and looted his quarters. They also freed all 100 elven bows. Lucidius Dies
* The group, after bear shenangins, traveled back to the Temple of Artemis with all 99 bows. Alex keeps the silencer.
* The group looks through the war plans, the battering ram, and the humiliation planned for General Aegeus.
* The group meets back up with Alleria, tells them what happened. Alleria informs them about the failed insertion point steal and all the captured elves.
* Alleria also tells them about Theseus, the "fool" son of Aegeus. He may be the best hope to get the king to join the Delian League and the rebellion. 
* The group messages Duplicamos and notifies him of the situation. They request that he makes a deal with the Delian League for reinforcements and retaking Artem. 

## A New Voyage
Late Summer, Year 218
* The group returns to Gelatin, Queen Anne's sister Seraphina arrives as per King Kenneth's request and volunteers to leave with the group.
* The group goes to an auction where Emmett steals a sapphire. Alex bought an invisible arrow.
* King Kenneth supplies the sloop "Queen Anne's Revenge"
* The group travels 3 weeks (old 6 days) to the [Ruins of the Hanging Gardens](/places/morea/hanging_gardens) on the [Morean Tip](/places/morea/morean_tip)
* They went to the Temple of Babel (where no speech works), met the priest, saw the chipped pillar
* The pillar appeared to be scorched by explosives
* The group traveled to see the crater of Babylon where it once stood. Their boat was stolen by looters.
* The group walked by a Reaper, left it alone, went to a guard house and got information.
* The group spoke with the Dryads, told them about the plan to burn down Artem heard from guards.
* The group went to Eros' clearing, met with Lucidius who is in love with Dandelion.
* Group embarrassed Barry the engineer, Lucidius failed to woo her, though she get agree to a picnic with Dandelion. 
* The group went back to the dryads, asked for hide & seek to distract Temple guards.
* The group killed all the guards with the help of some bloodthirsty guards
* The group gained the Honor of Artemis and were given access to the temple
* Spoke with Penelope of the Fletcher Rebellion, claimed help
* Three methods offered: Gather tactic information & supplies from Artem (strike at night, General knows about it). Get bows from the Stronghold. Hunt down the Chimera.

## Turre Antiqui
Mid Summer, Year 218
* Went to Turre Antiqui, opened up the 5-gem door by moving the gems
    Went through hall of beasts, Matthew stole 12 gold from illusion Dragon
* Made it through Hall of Righteousness, headed to Hall of Heroes.
 Chose peasant doesn't steal bread, guard thomas dies, didn't turn against each other
* The group made it through the Hall of Heroes
Chose to sneak by the weight room, Matthew changed to and fro, fought and beat Cereberus.
* The group made it through the Hall of Sin
Failed all three challenges
* The group listened in on the meeting between the [Gaia Initiative](/orgs/gaia_initiative).
* Quadarius managed to almost hit Silenus as he came to collect Hercules
* Quadarius managed to fool a spawn of the God of Trickery that he was dead.
* The group was caught by Duplicamos, he tried to bargain a deal. The group did not accept the deal. They were teleported out to Gelatin after tentatively agreeing. Each member has a mark of Duplicamos on their arm.
* Note: The group is actually inside fake Gelatin, and the mark is the bond to the illusion. They are still inside [Turre Antiqui](/places/gelatin/turre_antiqui)
* Alex was given [Juliet's Necklace](/items/juliets_necklace) by the [God of Trickery](/gods/dolos)
* The group broke out of the illusion after they noticed the black pearl still worked after tossing it, the strange response from Poseidon, the weird auctioneer, and then Alex figured out how to escape using Matthew's magic hand.
* Anne had escaped earlier when she noticed how off her husband was, and before she had a chance to escape, was confronted by Duplicamos himself.
* He escorted her to the top of a tower past Stuart the Alchemist. They spoke about his intentions (to build a new pillar of hercules over Cecily). He wished they would leave in peace.
* The group captured a guard, interrogated him, put him in a chair and wrapped him up. They were told that Duplicamos was on vacation for a couple weeks (plus succubus benefits for being a guard).
* The group masquerated as a group of guards. They met a guard that let them know about a side hussle (mining operation) and his past.
* After descending down to the mining operation and discovering the massive pillar of hercules that had been destroyed, the group fought Taskmaster Hazz and killed him (and a succubus).
* Krazzak encouraged Leucis to send a witch bolt through the orb killing every collar-based worker in control.
* They descended down the side of the mining operation dodging a bunch of demons running up the scaffolding. They went through the tunnel that is sending pillars to Cecily.
* They found Stuart the Alchemist who notified them that Duplicamos had company, thought it may have been them, told them a bit about the quiet tower. (sewers/tunnel transition).
* After going through a hall that had connections to a substantial portion of the tower, they found The Master Key. They turned it, and ended up meeting the Dumbwaiter after walking by the Beastmaster who was sleeping. 
* Krazzak explained the blood magic behind [the control orb](/magic/blood) secretly to the captain.
* The group ran into Pino Corona, a pineapple pianta, near the dumbwaiter.
* Duplicamos, who had been showing Queen Anne the tower, went to the prison with her to find companions.
* Duplicamos gave the group a sending stone (ruby) and told them about his duplicated tower and self. Did not learn about master key. He asked for their help to investigate the pillars of hercules.
* The group, while Duplicamos prepared a tour of the Cecily construction, explored his documents and the rooms of the different leaders of the Gaia Initiative. 
* Leucis finds a small control orb and keeps it.
* The group ran into the actresses at the Twilight Ampitheatre.
* Duplicamos met up with them, found out the tragedy of his miners, and showed the group Cecily's building of the marble.

## The Gelatin Vacation
Mid Summer, Year 218
* Spending time in the port city of [Gelatin](/places/gelatin)
* The Queen Anne has allegedly been abducted by pirates
* King Kenneth has requested that all adventurers available help
* Three places -- Pirate Caves, Wheat Farmers, Haunted Hills
* Recent rebellions between peasants and royalty over taxes
* Go to Bandit, make deal with Bandit to trade queen for princess
* Go to visit Stuart, helps provide shelter and notice to King.
* King starts giving out apology money after a fire in castle starts
* Go to Pirate cove, sign up to be bloodsail buccanneers
    find out pirates restarted 5 years ago, recent weird activity
* Fight witch through princess and captain, turn captain into water
(duplicated pirate ship spell)
* Go to Stuart, let him know -- there was a peasant revolt
    Left princess madison with him to heal
* King Kenneth put down peasant revolt, gave 3g to all peasants
* Quadarius released from jail
* Go to Bandit, made up between Pirates and Bandits.
    Left off at Bandit camp with Wizard Noah, Captain, Bandit King, Elizabeth
* Went to Gelatin, had a wonderful banquet and got reward for 2000g.
    Found out the voodoo doll used powerful blood magic, went to talk to Brandy.
		
        
## The Delfino Excursion
Early Summer, Year 218
* Appeared in [Delfino](/places/delfino) water portal after destroying the leader of Cecily.
* Met with Cop, cop asked to meet them in evening.
* Went to buxom wench after following lead from Wacho
* Stole from Buxom wench, got away by "turning in money"
* Got away to Ricco Harbor. Threatened woman selling medicare with Mafia.
* Joined Mafia (Emmett) and set out to do a boat heist to test mettle.
* Group eventually saddled up a boat and left the island.

## The Cecily Invasion
Late Spring, Year 218
* The crew set sail in Poseidon's Revenge, a powerful sea-worthy ship hailed by Leucis.
* Emmett found [The Odyssey Manuscript](/items/odyssey_manuscript).
* The boat was attacked at all sides, it was cut at the top.
* The crew was attacked by sirens.
* [Dolos](/gods/dolos) tricked the pirates into death. Offers help. The group refused.
* They landed at [Cecily](/places/cecily), the boat was sunk. They snuck through the caves, tricked orcs.
* The group defeated the Crown-wielder [(Elven Queen Patrician Silverstein's crown)](/items/silverstein_crown).


## The Invasion of Artem
Early Spring, Year 218
* The Despotate member [Argos](/places/morea/argos) invaded [Artem](/places/morea/artem) in year 218.

## The Delia-Morea "Delimore" War
Years 210-215
* The [Delian League](/orgs/delian_league) declared [war](/events/delimore_war) on the [Despotate of Morea](/orgs/despotate_of_morea) in year 210.
* While distracted, Poseidon destroyed the second [Pillar of Hercules](/places/pillars_of_hercules) under [Turre Antiqui](/places/gelatin/turre_antiqui) in year 214 before losing control of [Hercules](/demigods/hercules).
* Poseidon formed the [Dead Men Tell No Tales Agreement](/plans/dead_men_tell_no_tales) with Hades in year 215.
* Hades formed the [Gaia Initiative](/orgs/gaia_initiative) to secretly combat Poseidon.

## The Prophecy of Athens
Year 200
* The [prophecy of athens](/events/prophecy_of_athens) was made by [Oracle Delphi](/chars/oracle_delphi) at the [Temple of Apollo](/places/delia/temple_of_apollo) near [Apol](/places/delia/apol) in year 200.
* Angered by the prophecy, Poseidon enacted his [Atlantic War](/plans/atlantic_war) plans.
* The [Morean](/orgs/despotate_of_morea) city of [Knossos](/places/morea/knossos) assaulted the [Delian](/orgs/delian_league) city of [Delos](/places/delos).
* Poseidon destroyed the first [Pillar of Hercules](/places/pillars_of_hercules) in year 209, beginning his plan to [destroy the pillars of hercules](/plans/pillars_of_hercules_destruction). 
* Poseidon (through the pillar) [destroyed the hanging gardens](/events/hanging_gardens_destruction), attempting to divert the blame to Athena.
* Poseidon sent [Odysseus](/chars/odysseus) on a long journey since he was the only witness.
* Apollo, believing Athena a jealous liar, charged her denizens with war on [Athens](/places/morea/athens).

## The Sinking of Atlantis
Year 60
* Angered by the mortals rejecting him, Poseidon decided to take a group by force.
* Poseidon, against the wishes of the other gods, sunk the mortal city of [Atlantis](/places/atlantis) into the [Atlantic Sea](/places/atlantic_sea). 

## The Favor of Mortals
Year 30
* Poseidon and Apollo competed for favor of the mortals in [Delia](/places/delia) in year 100.
* Poseidon created a massive salt-water lake, mortals did not like. 
* Apollo gave them beautiful sculptures, mortals liked.
* The mortals of Delia favored Apollo.
* Poseidon and Athena competed for favor of the mortals in [Morea](/places/morea) in year 100.
* Poseidon brought fish to the seas, the mortals of Morea flourished.
* Athena brought people together to farm, they prefered grain to fish.
* The mortals founded [Athens](/places/morea/athens).
* Poseidon swore revenge upon the people of [Athens](/places/morea/athens) for rejecting him.

## Creation
Year 0
* [The world was created](/events/creation), year 0
* Zeus, Poseidon, and Hades decided to split the world in four. Zeus had the sky, Poseidon had the sea, and Hades had the underworld. The mortals could have the land.
