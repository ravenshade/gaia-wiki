<!-- TITLE: Reaper -->
<!-- SUBTITLE: Hades' Agents for Collecting Unwilling Souls -->

# Reaper
* [Hades](/gods/hades) employs reapers when [Hermes](/gods/hermes) cannot find all dead mortals to lead them to [The Underworld](/places/underworld).
* Reapers were a gift from [Persephone](/gods/persephone). They were converted from grain harvesting beings.
* Reapers are often used when souls go unburied, unwilling to head to [Charon](/gods/charon)

# Appearance
* Float several feet above the ground. Wear a long black robe. 
* Face is comprised only of a gigantic mouth. 
* Carry a long scythe used for ripping souls out of bodies.

# Items
* Carry the [Reaper's Scythe](/items/reapers_scythe)
* Carry the [Reaper's Sickle](/items/reapers_sickle)