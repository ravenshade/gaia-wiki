<!-- TITLE: Nymph -->
<!-- SUBTITLE: Spirits of the Rivers -->

# Nymph
* [See Wikpedia Doc](https://en.wikipedia.org/wiki/Nymph)
* Frequently hunted by [Saytrs](/races/saytr)

# Forms
* Naiads were associated with fresh water
* Oceanids were with saltwater
* Nereids specifically with the Mediterranean