<!-- TITLE: Undead -->
<!-- SUBTITLE: The condition of not living and living -->

# Undead
 * Immune to Necrotic (necrotic heals)
 * Radiant damage is doubled
 * Do not need to breathe
 * Do not need to sleep
 * Will not die if killed, but will not regrow any limbs.
 * Healing spells (unless stated) do not work.
 * Do benefit from short and long rests.