<!-- TITLE: Orcs -->
<!-- SUBTITLE: A quick summary of Orcs -->

# Orcs
 * Ritualistic, Shamanistic
 * Adopt emotion, strength, impulsivity over education, discipline, science, and logic.
 * Praise the gods Hecate, Hormes, Ares.

# Culture
## Mountainous Folk
Orcs are frequently found in mountainous regions, fighting against dwarves. 

## Dragon Worship
Orcs, due to their origin as servants of dragons, still tend to cling to worshipping these powerful beings. 
While most orcs see the gods as the true most powerful beings, many dragons are clever enough to convince the orcs that they are simply manifestations of them.

Many orc legends tell of ancestors who could dominate dragons, but rarely do orc tribes try to rise up against the dragons they worship.

## Slavery
Orcs believe that if you can subjugate another race, they do not deserve to be free. They frequently take prisoners as slaves.

Most commonly, it is seen as the pride of an orc warrior if they can subjugate a beast of much greater size, such as an Ogre, Ettin, or goliath. 