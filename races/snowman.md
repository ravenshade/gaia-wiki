<!-- TITLE: Snowman -->
<!-- SUBTITLE: An Ice Elemental from the Water Plane -->

# Snowman
**Ability Scores**: Con +2, Wis +1
**Size**: Medium
**Speed**: 30 ft, Swim 30 ft
**Age**: Up to thousands of years if well preserved, or seconds if not.
**Alignment**: Independent and self-reliant, Snowmen tend towards a neutral alignment depending on their summoner.
**Languages**: You can speak, read, and write Common and Primordial (Ice). Primordial is a guttural language, filled with harsh syllables and hard consonants.
**Size**: Snowmen are generally short in stature, usually around 5ft in height.

**Cold Resistance**: You have resistance to cold damage.
**Fire Resistance**: You have resistance to fire damage to avoid melting.
**Amphibious**: You can breathe air and water
**Swim**: You have a swim speed of 30 feet.
**Call to the Wave**: You know the shape water cantrip. When you reach 3rd level, you can cast the create or destroy water spell as a 2nd-level spell once with this trait, and you regain the ability to cast it this way when you finish a long rest. Constitution is your spellcasting ability for these spells.

**Info**: The lapping of waves, the spray of sea foam on the wind, the snowy cave, all of these things call to your heart. You wander freely and take pride in your independence, though others might consider you selfish. Some Snowmen have a deep desire to experience the warmth of the sun and must take measures to protect themselves lest they perish an early death. Snowmen are called to the material plane from the elemental plane by the desires of a child with a true heart and therefore match the alignment of their summoner. 