# Pianta

[[_TOC_]]

## Traits
* **Ability Scores**: +2 con, +1 cha
* **Size**: Medium (4-5ft)
* **Speed**: 30 ft

**Age**. Piantas fully mature about 5 years after they are harvested, but their incubation fruit period can take up to a decade. On average, they live 500 years. 

**Alignment**. Piantas tend to be good or neutral. They are laid back and friendly, but are short-tempered and defensive when they perceive a threat. They believe in a laissez faire attitude toward society, and are fiercely loyal within their familial communities or clans. 

**Size**. Piantas stand between 4 and 5 feet tall and average 130 pounds. They tend to be rotund. Your size is Medium.

**Languages**. Piantas can speak, read, and write Common and Pianti. Pianti mirrors their home islands; utterances mimic crashing waves, bubbling water, buzzing insects. Popping, smacking, and humming are commonly interwoven with bouncing stress patterns. It is a multitonal language with great variances in pitch to distinguish words.

**Grassroots**. Can cast *Speak with Plants* once per day. Charisma is your spellcasting ability for it.

**Natural Salesman**. Proficiency in Nature, Persuasion.

**Island Hospitality**. Proficiency in a tool of choice: a musical instrument, herbalism kit, brewer's supplies, potter's tools, or cook's utensils.

**Fruitarian**. Can only eat fruit or fungus.

**Photosynthesis**. Piantas gain enough nourishment for a day if they spend any time in sunlight or moonlight.

### Solar (Fruit-Based)

**Sunglasses**. Built in eye-protection from UV in bright sun. Advantage on saves against blinding.

**Shrine Sprite Soul**. The inner sun breathing life into piantas protects them from powerful radiant effects. Resistance to Radiant damage.

Solar piantas have multiple variations, some examples:

* Blueberry
* Strawberry
* Pineapple
* Orange
* Cacao
* Kiwi

### Lunar (Fungal-based)

**Darkvision**. Lunar piantas are accustomed to life under moonlight or underground. 60 ft.

Lunar piantas have multiple variations, some examples:

* Mushroom
* Mold
* Moss
* Lichen

## Info

![Pianta](https://mario.wiki.gallery/images/thumb/7/71/MSS_Pianta_Artwork.png/250px-MSS_Pianta_Artwork.png "Pianta")

Piantas are rare fruit and fungal based sentient beings with a relaxed and energizing demeanor. Piantas prefer long stretches of beaches, hot-hot days, the calming pull of the ocean, and a delicious bowl of fruit. They are known for their destination villages, lighthouses and nighthouses, lassiez faire practices, strong trade and merchant skills, the long lasting and gorgeous twinkling of their shine and lunar sprites throughout the year, and their quick-to-anger penchent for unruly foreigners. Despite living in small communities far apart in islands, piantas are united through shared rituals, religious practices, their gluttunous love of fine food and luxury, island hopping sailboats, trade routes, alcoholic drinks, and desire for world tourism. 

#### Solar

Descended from native fruit bushes and trees, solar piantas are most commonly found on sunny island chains. Most solar piantas spend their days in gardens growing food, building entertainment venues, relaxing at the beach, or making cut-throat trade deals for margins of luxury goods. Solar piantas have bright colors based on the fruit they came from, such as dark blues, light blues, bright oranges, deep reds, neon greens. Solar piantas are charismatic folks and gladly will embrace anyone into their homes, but will not tolerant dirt or rudeness. 

Most solar piantas view their origin as the community they were harvested into, but the native home of the first piantas and shrine sprites is the dolphin-shaped Delfino Isle, located in the middle of the great oceans of the continent. 

#### Lunar

Descended from fungus and mushrooms, lunar piantas are typically found in dark cloudy and moist regions, often inside caves near the surface, or in the underdark in rare circumstances. Lunar piantas prefer the shade and are known for their 'nighthouses' that create magical darkness or artificial night even during daylight. Most lunar piantas live their life similar to their solar counterparts, except they prefer to spend more time on crafts and specifically abstain from the gluttunous habits and greed. Life either underground or in moonlight has given them special adaptations to handle the dark. Lunar piantas have equally beautiful gardens, highlighted through biolumniscent ink from native fungi and tunnels to allow moonlight inside. 

Due to their love of alcohol, long life times, underground tunneling, and lack of greed, lunar piantas are close allies with many dwarven kingdoms. Lunar piantas often find themselves within dwarven strongholds, working as merchants or craftsmen, running inns, hospitals and casinos, or providing essential sources of food.

### Bulbous, Colorful, and Bouncy

Rotund and colorful, piantas are known as fantastic merchants, gardeners, hospitality workers, and craftsmen. While piantas are shorter than most humanoids, they always stand out with their bright array of saturated skin tones and protruding tree or mushroom sprouts from their heads. Piantas prefer to soak as much sunlight as possible, so rarely do they wear more clothing than a grass skirt. Their spherical shapes extend to their hands and feet, with only two toes and a palm and finger. Despite their large and chubby appearance, piantas are suprisingly light on their feet. Based on plants, the race is almost entirely hairless, albeit having to trim the leaves of their palm fronds or mushroom sprouts occasionally.

### From Seed to Harvest

Piantas can live greater than half a millenium, but few piantas view death as the end of living. In fact, most piantas willingly choose the time of their death, when they felt they have lived long enough and wish to join 'the garden' in tandem with their spouse. While every pianta community is different, nearly all revolve around a sacred garden or set of plots where elderly piantas are buried. This garden is meticulously cared for by the community and is usually the site for multiple essential rituals and festivals. It is the spiritual home, the place of death, the place of birth. Surrounding the gardens are swarms of sprites, the ancestor souls of the community's elder piantas, long past.

The buried pianta (stem facing up, above ground) will decompose, and its body germinates and grows roots into the soil. Presuming the new sprout-top is cared for, given adequate water and light, a sapling of a pianta tree will grow based on the DNA of the buried elder. A sapling can take nearly a century to mature into a full tree, which resembles either a large mushroom (lunar) or palm tree (solar). Saplings are planted in pairs, as Piantas are strictly monogomous (a male and female).

As part of maturation, the root systems between the pair of saplings grow intertwined, and DNA is exchanged. Both trees can begin to produce fruit for the matching sex (male-male, female-female). Fruit typically takes a decade to ripen and requires meticulous care and light throughout. A Pianta tree couple typically produces a harvest of 6-8 newborn pianta fruits (3-4 of each sex) over their lifetime. Newborn Piantas are cared for by the surviving families and mature into adults within 5 years of harvest. 

When a pianta tree couple has finished producing offspring fruit, they begin to wither and lose their leaves. A shine sprite (or lunar sprite), a small glowing star (or moon), is released from both trees in tandem. These Sprites are considered the soul of piantas, ancestor spirits that protect the island, produce beautiful radiant light, and guard the garden. These sprites assist with caring for the newborn fruit and are considered essential to receive enough light to bring a fruit to harvest.

Culturally, a newborn Pianta is seen as a community-wide celebration because of its rarity. Large communities of ancestral sprites are worshipped by living Piantas and are often contacted for guidance. The beautiful radiant energy from the sprites gives most Pianta villages and gardens an illuminuous sparkle.

### Culture of the Islands

Piantas mirror their islands; they flow like water. They are social chameleons, rarely challenging others on their beliefs, so long as they are not challenged. They are quick to anger, quick to calm, laugh heartily, forgive fast, and curse ruthlessly. Piantas believe hospitality comes first and treat their clients with luxury. Since Pianta lives are so long, family is seen as pivotal. Their long lineage and guardian ancestors are given uptmost respect. Children are prized. Above all, piantas search for a good time. 

However, as a society, they have a lassiez faire attitude and libertarian viewpoint on structure and rules. Piantas are passionate about their viewpoints on kindness and giving on a community level, but prefer not to formalize any systems. Because of this preference for internal mediation, pianta communities tend to stay relatively small, small enough that every pianta knows who lives in the village and can trust them. Piantas typically welcome foreigners to their islands, no matter their racial background or opinions, but respond to aggression toward their culture and shine sprites with full force. 

Since their ancestors' souls inhabit their islands and gardens, Piantas maintain their ancestry and traditions over milleniums. Rituals often date back to the original sprites who established the practice. Despite living far apart, piantas share their culture and rituals with little divergence, except for when disagreements took place. Most piantas eschew the ideas of social status and hierarchy and have ritualized the practice of communal sharing of farming, wealth, and childcare. Families cherish their unique individuality of traits, however, and often operate differently from the community as a whole. Worship of family or community ancestors, often treating them as a personal pantheon, is common.


### Wanderlust, Luxury, Ancestry, and Fun

Piantas who take up the adventuring life are might be driven by an innate wanderlust to explore new undiscovered islands or territory. From their libertarian and island-hopping roots, even the most relaxed and home-bodies of piantas enjoy independent tourism. Other piantas may be motivated by their gluttonuous instincts to find greater luxuries in the world to enjoy, often with an altruistic desire to improve the lives of their home community. Ancestry and spirituality are other important motivators. A pianta may receive a calling to found a new pianta community from their ancestor sprites, or find new trade routes for their current community, or even establish new gardens. Piantas may go on decade long pilgrimages to visit other pianta gardens to rekindle friendships from centuries or even lifetimes ago. Love and relationships might also motivate a pianta to travel, in hopes that they may find a life partner that they will one day be buried with. Lastly, piantas have long lifetimes and want to have as much fun as they can, perhaps they are simply seeking stories, or wish to bring more kindness into the world.

Regardless of their reasoning, all piantas wish to pass from old age, buried in their family's garden. Piantas that travel far and wide always try to find their way home in the event of an injury or in the rare case of a mortal wound.

### Names

First names are given by the community when a pianta is harvested. As part of the harvest ritual, druidic piantas commune with the fruit-bearing pianta tree or nearby ancestral sprites to ascertain their desired name.

Piantas usually have family names based on the fruit or fungus they descended from, with a variation based on the genetic line or cultivar they come from. Cross-germination is an important part of pianta culture, so last names are typically the highest level common descendent rather than the entire lineage. In deeper lineages, signature crosses between fruits (often from alcoholic drinks) are used.

Pianta names are typically androgenous. Often similar to modern day Somoan or Pacific Islander names.

##### Solar

| First  |         |       |      |       |
|--------|---------|-------|------|-------|
| Lagi   | Hesper  | Pepo  | Pina | Fiva  |
| Mai    | Maitaki | Alofa | Hemi | Noa   |
| Tala   | Kai     | Anaru | Lono | Taika |
| Malibu |         |       |      |       |

| Family    |           |           |            |             |
|-----------|-----------|-----------|------------|-------------|
| Apricot   | Saccharum | Sunflower | Mangifera  | Pomegranate |
| Muskmelon | Guava     | Corona    | Colada     | Daiquiri    |
| Amaretto  | Boysen    | Currant   | Achene     | Cypsela     |
| Drupe     | Samara    | Caryopsis | Watermelon | Strawberry  |
| Grape     |           |           |            |             |

##### Lunar

| First   |        |        |         |        |
|---------|--------|--------|---------|--------|
| Taki    | Luni   | Kiri   | Azureus | Celium |
| Agaric  | Azure  | Azura  | Morel   | Cap    |
| Buttons | Brandy | Trog   | Rye     | Rosato |
| Mani    | Sutaki | Bolete | Russula | Nuda   |
| Nameko  | Delika |        |         |        |

| Family     |             |          |          |           |
|------------|-------------|----------|----------|-----------|
| Portobello | Maitake     | Shiitake | Buttons  | Crimini   |
| Porcini    | Chanterelle | Enoki    | Polypore | Lactarius |
| Blewit     | Bolete      | Resinous | Kahlua   | Campari   |
| Amaretto   |             |          |          |           |

