<!-- TITLE: Pleiades -->
<!-- SUBTITLE: A quick summary of Pleiades -->

# Pleiades
 * "star nypmhs"
 * https://en.wikipedia.org/wiki/Pleiades_(Greek_mythology)

# The Seven Sisters
 * Maia (Μαῖα), eldest of the seven Pleiades, was mother of Hermes by Zeus.
 * Electra (Ἠλέκτρα) was mother of Dardanus and Iasion, by Zeus.
 * Taygete (Ταϋγέτη) was mother of Lacedaemon, also by Zeus.
 * Alcyone (Ἀλκυόνη) was mother of Hyrieus, Hyperenor and Aethusa by Poseidon.
 * Celaeno (Κελαινώ) was mother of Lycus and Nycteus by Poseidon; and of Eurypylus also by Poseidon, and of Lycus and Chimaereus by Prometheus.
 * Sterope (Στερόπη) (also Asterope) was mother of Oenomaus by Ares.
 * [Merope](/chars/azul) (Μερόπη), youngest of the seven Pleiades, was wooed by Orion. In other mythic contexts she married Sisyphus and faded away. She bore Sisyphus several sons.