<!-- TITLE: Dryad -->
<!-- SUBTITLE: Spirits of the Trees -->

# Dryad
* Predominately live in the forests surrounding [Arcadia](/places/morea/arcadia) and [Artem](/places/morea/artem)
* [Artemis](/gods/artemis) is known to be the most friendly to Dryads.