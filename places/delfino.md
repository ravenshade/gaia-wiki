<!-- TITLE: Delfino -->
<!-- SUBTITLE: Isle Delfino, tropical island. -->

# Map
![Delfino Isle](/uploads/delfino-isle.jpg "Delfino Isle")

# Cities
* [Delfino Plaza](/places/delfino/delfino_plaza)
* [Ricco Harbor](/places/delfino/ricco_harbor)
# Inhabitants
* [Piantas](/races/pianta)
* [Nokis](/races/noki)
* [Mantas](/races/manta)
* [Bloopers](/races/blooper)
* Pirahna Plants
* [Cataquacks](/races/cataquack)

# Economy
* "Reds, Blues, Sprites"
* Red Coins: Medium-sized red coins burnt by the sun
* Blue Coins: Small little coins colored by water magic
* Sprite: Living metal creatures, emit slight light. (eats blue/red coins. Fly around. Taboo to capture)
* Luna: Opposite of sprite, emits darkness.

# Culture
* Strong battle between light and dark.
* Tension between Piantas and Nokis
* The bolbous of Piantas are not to be discussed.

