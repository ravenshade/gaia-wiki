<!-- TITLE: Gelatin -->
<!-- SUBTITLE: The Island of Gelatin -->

# The Island of Gelatin
# Map
![Gelatin Map](/uploads/gelatin-map.jpg "Gelatin Map")
# Organizations
* The [Kingdom of Gelatin](/orgs/gelatin_kingdom)
* The [Bloodsail Buccanneers](/orgs/bloodsail_buccanners)
* The [Gelatin Bandits](/orgs/gelatin_bandits)

# Locations
* [Bandit Camp](/places/gelatin/bandit_camp) (Haunted Hills)
* [Pirates Cove](/places/gelatin/pirates_cove)
* [Gelatin Farms](/places/gelatin/gelatin_farms)
* [Turre Antiqui](/places/gelatin/turre_antiqui) (Haunted Hills)

# Landmarks
* [Guard Thomas](/chars/guard_thomas)' Tower
* [Stuart the Herbalist](/chars/herbalist_stuart)'s Hut