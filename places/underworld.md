<!-- TITLE: Underworld -->
<!-- SUBTITLE: The Realm of Hades & Persephone -->

# The Underworld
* [See wikipedia doc](https://en.wikipedia.org/wiki/Greek_underworld)

# Map
TODO
# Notable Residents
* [Hades](/gods/hades) - King of the Underworld
* [Persephone](/gods/persephone) - Queen of the Underworld
* [Hermes](/gods/hermes) - brings souls to the Underworld
* [Charon](/gods/charon) - Gatekeeper with a boat that brings people across the River Styx

# Organizations
# Locations
### The Shade Library
A library that contains scrolls written by Shades.
These scrolls contain all details of mortal lives. Changing the scrolls changes the mortals lives.
# Inhabitants
# Economy
# Culture