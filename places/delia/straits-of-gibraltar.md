<!-- TITLE: Straits of Gibraltar -->
<!-- SUBTITLE: The Rocky Darkened Shores -->

# Straits of Gibraltar
 * The Straits of Gibraltar are a rocky, hazy, dangerous set of barrier islands and sandbars. A place where droves of outcasts run and hide in the shadow of the rocky shores. The locale is inhabitated by a myriad of races -- $(races). But monsters, too, lurk amongst the rocks. Gryphons, dragons, and orcs are plentiful. This is a place where the gods turn a blind eye.
 * The discovery of gold in the Dragon tooth mountains have brought a flock of outsiders to the straits. Eager adventurers seeking fortune, rich aristocrats seeking to double their own, parasites seeking a sturdy host, and native dwellers fighting to preserve their livelihoods.
 * Incredibly rocky shores. Frequent barrier islands. Sandbars.
 * Entire area has a haze and fog trapped between the rocks and mountains on the straits.
 * Land is dark most of the time. Hinterlands-like terrain or darkshore-like terrain.
 * Bottom half of the area is permanantly night because of the Nighthouse.

# Map
![Straits Of Gibraltar](/uploads/maps/straits-of-gibraltar.png "Straits Of Gibraltar")

# Inhabitants
 * Kenku
 * Tabaxi
 * Tortle
 * Goblin
 * Firbolg
 * Half-Orc
 * Orcs
 * Tiefling
 * (slightly rare) Human
 * (rare) Pianta

## Monsters
 * Gryphons
 * Orcs
 * Black Dragons

# Quests
## North Snaghead
### (done) Gibble the Goblin Pet Merchant
Selling rare and exotic pets. 
- Pets are actually polymorphed travelers who spend the night in Shellrock Inn

### Orc Problem
Orcs from Kor'Kash usually peaceful, been attacking nearby travelers and villagers.
- Given by Adrian Gorgolloth, an adventurer who's entire party died to the dragon inside the mountain.
- Dragon in Kor mountain has come awake because its pillar was disturbed.
- (secret) Adrian Gorgolloth plans to join the adventurers to potentially save his crew.
- (secret) His crew is mostly alive, if they are all rescued, will attempt to gain the acclaim. Going under the name ['Patrons from Patras.'](/orgs/patrons-from-patras)

### "Moonfire" The legendary bow of Selenis
"Curious Shadow" tabaxi ranger has an old frayed map with the location of this legendary bow.
- "Curious Shadow" is one of the thugs that Emmett scared off.
- Leads to the Temple of the Stars, it was stolen by off-coast whalers.

## South Snaghead
### A Concerned Cartographer
Foley, a kenku cartographer, wishes to produce a map of the straits of gibraltar
Particularly interested in mapping out the pirates along the standard route.
- needs adventurers to keep him safe. In exchange, will provide free copies of map.

### A Dark Dwarven Promise
Brox, legendary blacksmith, traded his soul to a devil in exchange for unearthly abilities.
After a long life, he wishes to pass on to the afterlife. Can't seem to die, has tried.
 - The Devil lives in the Haunted Pastures. 

### The Flaying Curse
Long ago, Snaghead once existed by a large lake to the south. The city became full of hubris,
did not sacrifice to their patron god Hecate. She bestow a curse upon them, causing skin to flay.
Galu the tortle wishes to break the curse on the land, even after all these years.
- Curse was caused by the necromancer Azul
- Technically in cahoots, protecting the pillar where Old Tower is from cultists.

### The Cult of Poseidon
Hostile city Seahawk. Sacrificing adventurers from all over to Poseidon.
Known for throwing people into the Vortex.
Vortex has become unruly, townsfolk have heard stories of it sucking people in from shore.
- Cult of Poseidon is summoning the kraken gods from deep below.
# Locations
## Colchis
 * [Colchis](/places/delia/straits-of-gibraltar/colchis)
 * Aarakocra && Kenku city. Fabulously wealthy from mining the rocks around the straits. 
 * Kenku are slaves to the Aarakocra
 * City is enveloped in ritual. 

## Jak's Landing
 * Goblin town famous for its massive observatory. The local Nighthouse keeps the area permanantly dark.
 * These goblins frequently work with the city of Colchis. Many are inhabitants. Slave trading with Sparta is common.

## North Snaghead
 * Small city with a mix of goblins, humans, kenku, goliaths, and Tortles. Racial tensions. 
 * Frequented by traveling goblins with their enslaved goliaths for trade.

#### Snail Trail Inn
Owned by Old Man Tortle. 

#### Shellrock Inn
Owned by Old Man Glurn. Glurn sells the famous Shelldrop soup. Main tavern/inn in town, most people stay here.

#### Slim Shady the Goblin
A goblin that owns 2 goliaths. Owns a stall in town, regularly sells cheeses and other 'exotic' goods.

#### Thunk the Map Merchant
Kenku. Used to be a slave. Permanantly damaged vocal cords. Name is the literal 'thunk' sound.

#### Kraggle's Lighthouse
Kraggle the Orc Lightkeeper owns a lighthouse. Respects piantas. Wishes he could have shine sprites of his own, struggles with the fog.
Friendly with the party.

#### Sees-No-Water (dead)
Tabaxi thief. Attempted to steal from the party and was killed by Emmett Baldir. His friends all hate the crew.

## South Snaghead
 * Small city with Tabaxi, tortles, tieflings, goblins, and kenku. Racial tensions.

## Rumblood City
 * Lasseiz-faire run amuck. Pirate-run city.

## The Graveyard

## Living Forest
 * Large Druid Grove. Birthplace of Firbolgs. 
 * The forest rejects nonnatural forces placed upon it. All structures are destroyed, including tents.
 * The trees are alive. Perhaps all controlled by one druid.

## Kor'Kash
 * A mountain range dominated by small orc clans. The orcs mostly leave outsiders alone, but recently have been lashing out at anyone.

### Kor Mountain
 * Contains Pillar of Hercules
 * Red Dragon sleeps within the mountain, guarding the pillar. 

## Temple of the Stars
 * Lunar Piantas, descended from mushrooms and dragonfruit. 
 * Order of the Stars monks run the temple and the local Nighthouse.
 * Local firbolgs aid in running the temple.
 * The Star of Selene is hidden deep inside.
 * Cult of Poseidon plans to invade the temple and steal the star. 

## Fir
 * Firbolg city. Large wooden log cabins and small caves make up a sizeable firbolg civilization.

### Shrine to Selene
 * Dedicated to moon goddess
 * Shrine looked after by local Firbolgs.
 * The Staff of Selene was stolen from it and taken to Cecily by Morean soldiers.

## The Vortex
 * Contains [Charybdis](/mobs/charybdis) and [Scylla](/mobs/scylla) in a somewhat dormant state.

## Cyclops Isle
 * Contains the legendary [Cyclops](/mobs/cyclops) from the Odyssey.

## Calypso's Isle
 * Trapped Odysseus for years.
 * Owned by the nymph, Calypso.

## Cursed Lands of Miletus

### Undead Creatures
 * Skeletons, Zombies
 * Banshee
 * Dryad Spirit
 * Reapers (hb)
 * Geist
 * Ghost
 * Wraith
 * Allip (from discovering secrets)
 * Undead Tree
 * Giant Skeleton
 * Sword Wraith Commander
 * Knights of Miletus (hb) - (Nightveil Specter, Death Knight, Knight of the Order)


### Azul's Manor
A hidden manor that is haunted on inspection, lines up with a manor on a Demiplane that is full. 

# History
 * Was once part of mainland Delia before it was torn asunder by a great earthquake.
 * The territory was also home to dwarves, though none remain anymore. 

## Map of 176 AD Gibraltar
![Straits Of Gibraltar 176 Ad Sm](/uploads/maps/straits-of-gibraltar-176-ad-sm.jpg "Straits Of Gibraltar 176 Ad Sm")
