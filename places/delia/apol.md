<!-- TITLE: Apol -->
<!-- SUBTITLE: A massive trade city on the Delian River -->

# Apol
* Founded for the god [Apollo](/gods/apollo)
* Ruled by [King Periphas](/chars/periphas)
* The land surrounding is generally referred to as Attica

![Apol](/uploads/apol.jpg "Apol")
# Map
TODO

# Worship
 * Apollo
 * (His Father) Zeus
 * (His Mother) Leto
 * (His Sister) Artemis
 * The residents tend to hate Hera for banning Leto from giving birth on mainland
# Organizations
* Capital city of the [Delian League](/orgs/delian_league)
* Led by the [Candora Aristocracy](/orgs/candora_aristocracy)

# Notable Residents
* [Okianos](/chars/okianos) - friend of Kreios
* [Theia](/chars/theia) (deceased) - lover of Kreios
# Locations
## Heliacan Colosseum
* Champions: Priscus (shieldwarden), Crixus, Marcus, Spartacus
* **Rules**: Magic & potions not allowed, but used like modern steroids. Frequently abused and kept secret.

## Apol Stock Exchange
* Located on the Yellow Brick Road

## Apollo's Heart
 * Massive mini-sun in the middle of the city. Clerics of the Order of Apollo maintain the sun and the 

## The Isle of Leto
 * Supposedly the island off the coast where Leto gave birth to Apollo and Artemis.
 * Statues of Rhea look down over the island, watching carefully, protecting from other goddesses such as Hera, Demeter, and Aphrodite.