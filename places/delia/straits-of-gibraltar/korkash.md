<!-- TITLE: Korkash -->
<!-- SUBTITLE: The Mountains of Kor'kash, an Orc haven -->

# Kor'kash
 * A range of foggy, swampy mountains.
 * Mount Kor is full of flooded dwarven ruins. 
 * Covered in dragon effigies and skull posts.

# Map

# Notable Residents
### (dead) Chieftain Wobagda
A fiercesome orc. Fighter. Has a hook replaced for a missing hand. Missing an ear. Commands a tortured ogre.

Abilities: Action Surge. Disarming Attack, Goading Attack. Parry. Riposte. 

Died from the party in the first attack.

### (dead) Deathwarden Shagar
The keeper of the prisoners that will be sacrificed to Hormes. Frequently near the Ritual Circle. Is a warlock, has made a pact with Hormes.

Spells: Toll the dead. Green-flame blade, Darkness, Crown of Madness, Blight, Fly. Can raise undead.

### (dead) Seer Yotul
A wise elemental bender. Known for making prophecies about travelers passing through. Saw the party coming. Blind (tremorsense). 

Tends to the spiritual needs of the God, Hormes. 

Spells: Primal Savagery, Poison Spray, Thorn Whip, Grasping Vine, Move Earth, Wall of Fire, Wall of Water, Fog, Lightning Bolt, Erupting Earth, Far Sight. 

Died from the party in the second attack.

### (dead) Sage Phyrus
A young shaman who wields two two-handed maces with the power of the wind. Impulsive. Well respected.

Spells: Misty Step, Scorching Ray, Hold Person, Maelstrom, Haste, Reincarnation.

[Shaman homebrew](https://homebrewery.naturalcrit.com/share/SJ-g9ViVJZ)

Died from the party in the second attack.

### Daru the Druid
A Tortle druid that is now covered top-bottom with fungus and spores. 

See [Daru](/chars/daru)

Spells: Infestation. Snare. Blindness, Animate Dead, Gaseous form, blight, confusion, cloudkill, contagion. Giant Insect. Insect Plague. Fog.
Abilities: Halo of Spores (necrotic damage around you). Symbiotic Entity. Fungal Infestation (animate corpses).
# Encounters
## Orc Ambushes
 * Several groups of orcs among the swamps will attempt to attack the adventurers
 * Orcs, Orc War Chiefs, Orc Shamans.
 * Orc Drummers (bards), Orc Barbarians, Orc warlocks. 

## Hostile Creatures
 * Creatures such as giant crocodiles, stirge, Will-o-Wisps, or bullywugs may attack the party.
 * More difficult: Hydra, Shambling Mound

## Daru Hags
 * There are plenty of Green Hags around the Daru thicket. 

## The Daru Druid
 * The [Daru Druid](/chars/daru)
 *  At the center of the Daru Thicket is a Moon Druid that has gone mad.

## The Patrons from Patras
 * Kiwi is lost in the Daru Thicket
 * (freed) James Bonnet has been captured and is going to be sacrificed in the Ritual Circle.
 * Carrie is lost in the Kor mountains
 * Theed Latto is being kept by orcs in Old Kharanos as bait.

## Hormes, the Adult Black Dragon
 * [5etools](http://5etools.ravenshade.com/bestiary.html#adult%20brass%20dragon_mm)
 * Tended to by residential shamans. 
 * Given Goliaths to sadistically torture. Given food that they get from ambushing goblin merchants along the road.
 * At the Kor Summit
 * [Hoard & Rewards](/notes/hormes-hoard)
# Locations
### Rickety Bridge
A bridge that leads into the swamp. It's falling apart. Dangerous to cross. 60 feet long, rope ready to snap.

### Kor'krash Swamp
A massive wetland and swamp. It has large drooping willow trees. The water can range from a shallow couple inches to a deep 10 feet. Gets water from both sides of the strait.

Many small orc settlements exist inside the swamp, usually temporary. Ambushes are possible.

### Billy Bob's Grotto
The grotto where Billy Bob must have ended up. You can find some cooking supplies, evidence of Chops, and some notes about his travels.

### Hormes Statue
A massive obsidian statue of a dragon in the middle of a swamp.

### Krag'nush
A large orc settlement near the Ritual Circle. Inhabited by the majority of the orcs in the region.

 * The settlement was entirely destroyed by the Patriots.
 * The settlement was burnt by a fire elemental.
 * The settlement was soiled by a Reaper and the gross revival of Leucis
 * The settlement will never recover and will always be a scarred land.

### Ritual Circle
A set of stones that are used for ritual sacrifice before sent to the Kor mountains for disposal.

### Shaman Ruins
Nearby the ritual circle, contains the majority of the wise shamans of the clans. 

### Ol' Bunker
An old dwarven bunker that is now inhabited by an orc tribe. 

### Ol' Kharanos
An abandoned and delapidated dwarven town. Inhabited by smaller groups of orcs, usually those who are hunting individuals passing by.

### Flooded Quarry
An old dwarven mining quarry that has been completely flooded. Mostly abandoned.

### Daru Thicket
A dark and evil thicket of briars. An insane fungal druid from The Living Forest lives at the heart of the forest.

### Dwarven Graveyard
An old graveyard from when the dwarves were at their height. Statues of famous kings lay there.

Kings: Brox Badune, Bartom Badune, Selasta Badune

### Kor Mountain
 * Adamantite/Thorium Forge in the center, potentially restorable
 * Mostly flooded


# History
## The Great City of Kor
 * Once a great dwarven city before the Straits broke off from the main land, before they were flooded by the oceans.
 * During the height of their civilization, Hormes claimed the mountain.
 * Hormes found the great pillar with a dwarven throne atop it. He shook the pillar, causing the land to reap and sow apart from the main land.
 * He was scared off, but the wreckage of the island caused massive flooding. The dwarves struggled to grow food. Mining became an impossible task.
 * A nearby roaming group of orcs began to take advantage of their struggles and pillaged isolated villages and cities.
 * When they were weakened, Hormes returned to destroy the last of the dwarves. He then made a nest there to live in.
 * The orc encampments settled in the abandoned homes of the dwarves, and worshipped Hormes for giving them a home.
 * The swamp has now set in for around 70 years. 
 * The Patriots of Patras killed Hormes the dragon, the swamp will begin to abate. Daru will heal the land.
