<!-- TITLE: Turre Antiqui -->
<!-- SUBTITLE: The Ancient Tower  -->

# Turre Antiqui
* The Tower was constructed in year 10
* Marked the Center of [Gaia](/places/gaia)
* Built above the first [Pillar of Hercules](/places/pillars_of_hercules) that was destroyed by Poseidon.
* It is two towers - One is duplicated onto the other. "The Master" and "The Apprentice" towers.
* The two towers overlay, which means that swapping between them 1-1 is possible -- requires [The Master's Key](/items/turre_antiqui_masters_key).
* The tower was created in dedication to [Dolos](/gods/dolos)

# Gaia Initiative
The [Gaia Initiative](/orgs/gaia_initiative) took over the Apprentice's tower in an attempt to repair the pillar.

The tower is technically owned by [Duplicamos](/chars/duplicamos). 

# Map
![Turre Antiqui](/uploads/maps/turre-antiqui.png "Turre Antiqui")

# Notable Residents
 * [Dumbwaiter](/chars/dumbwaiter) - An awakened Dumbwaiter that can move about the tower. 
 * [Actress Velatha](/chars/actress_velatha) - (Elven) Former lover of Duplicamos. He told her she would be a great star. Naive.
 * Actress Christine - Actress who works with Velatha. 
 * Warden Brono - (Dwarf) Rugged nostalgic warden. 
 * Guard Nina Dewfoot - (Halfling) The only halfling guard Gelatin ever hired, disappeared to tower. 
 * Miner Dave - Hard-working laborer with a heart of gold. Isn't mind controlled.
 * [Stuart the Alchemist](/chars/alchemist_stuart) - Duplicated version of [Stuart the Herbalist](/chars/herbalist_stuart). 
 * Ampitheatre Phantom - Ghost, turned actor, has been out of work for a couple decades.
 * Crystal the Succubus - (Succubus) Lives in Hall of Sin, used to run the place. Wife of Taskmaster Hazz.
 * Beastmaster Krag - (Furry Orc) Lives in ruined Hall of Beasts. Owner of the [Beastmaster's Whistle](/items/beastmasters_whistle)
 * Taskmaster Hazz - (Saytr) Runs the Gaia Initiative Collection operation. Loves whips.
 * Impmaster "Impmother" Snezu - (Succubus) Conservative Succubus. Mother cursed by Dolos. Runs kitchen like a mom to her imps.
 * Knob & Cob the Talking Door - (door) Guards the library. Requires a "gameshow" of trivia to enter.

# Apprentice's Tower

### Entrance Hall
A duplication spell has been tapped that requires the user to make a "360" turn through the front door twice.

### Room of Beasts
A room with enchanted puppies that look like young red dragons.

### Hall of Righteousness
A series of difficult tasks involving courage, humility, and selflessness

### Hall of Heroes
* Hall of Gallantry
Fight Cerberus in the false Hercules Arena. Matthew found [Cerberus' dagger](/items/cerberus_fang).
Kill him, turn into black lab. Black lab follows 
* Hall of Change
Matthew changed to Mountain, changed back.
* Hall of Strength
Matthew opened with moving ball instead of lifting 6 rock dumbbells

### Hall of Sin
Supposed to fail.
* Hall of Greed - greedy action, failed in execution
* Hall of Lust - resisted lust
* Hall of Murder - shoot the fireball, chose that it was not murder

### Hallway of Choice
* Cauldron
* Forge
* Chambers (major 4 + hercules + meeting room)
* Doll

### Meeting Room
* Main room where they meet. Giant map. Portals. Big desk. Top of a tower.

### The Room of Fire
It's a room entirely filled of fire.

### The Room of Control
* Full of voodoo dolls from people around town
* Full of musical equipment and small acting props.

### Surveillance Room
* Full of mirrors/balls to observe parts of the tower

### Hall of Experiments
* Small room full of duplication experiments

### Alchemist's Room
* A room full of alchemy equipment
* Home of the [Tailor's Potion](/items/tailors_potion)

# Master's Tower
### The Pillar of Hercules
* One of the major pillars of hercules (destroyed)
* Dozens of imp, undead, and human miners collecting pieces. 

### Twilight Ampitheatre
* In disarray, has not been used in a long time. Been converted to storage. 
* Has backrooms with various actors, actresses.
* Famous for its production of The Book of Athens and The Phantom of the Ampitheatre

### Hall of Experiments -> Hall of Morphing
* Failed experiment, accidentally duplicated the tower.
* Duplications are constantly spontaneously appearing, disappearing.
* The walls and all objects morph uncontrollably.

### Ruined Halls of Sin, Heroes, Beasts
* Similar to the Apprentice's tower, but completely destroyed.

### The Warden's Enclave
* A set of prisons and magical prison. Most of which is destroyed.
* Run by Warden Brono.

### Servant's Quarters
* Homes of the servants and now pillar laborers.
* Most of the servants are magically enchanted objects.
* Contains a healer's chambers. 

### Kitchen
* The only room that extends between the two towers. 
* "Food is difficult to duplicate."
* Run by Impmaster Snezu.

### Dining Hall
* Always a massive feast kept on the table.

### Music Room
* Full of instruments, can automatically play.

### Ballroom
* Cursed room. Everyone dances poorly in the ballroom.

### Dressing Room
* Full of mirrors (maps to Surveillance Room).
* Servants can automatically change your appearance.
* Contains a large store of [Tailor's Potions](/items/tailors_potion)

### The Training School
* Previously used by guests to learn about Illusion magic
* Has not been in operation in many years, dusted and unused.

### The New Hall of Experiments
* Portal to the chambers inside
* The Gameshow of Trivia hosted by the Talking Doors.

### Master's Chambers
* Duplicamos' (master) study, bed, artifacts
* Has a magical sky that shows the night, moon
* Contains the Master's Key
* Shrine to Dolos, Hades.

### The Observatory
* Legendary Telescope "I Scry with my Little Eye" (allows user to magically scry on one person once per day)