<!-- TITLE: Babylon -->
<!-- SUBTITLE: The City of the Skies -->

# Babylon
* Originally located in [Babylonia](/places/morea/babylonia)
* Teleported into the sky during the primal invasion in the [Delimore War](/events/delimore_war)
* Ruled by [King Nebuchadnezzar](/chars/nebuchadnezzar)