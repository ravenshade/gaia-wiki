<!-- TITLE: Delfino Plaza -->
<!-- SUBTITLE: The Metropolitan capital of Isle Delfino -->

# Citizens
* [Clemen Times](/chars/clemen_times) - mayor, orange, long beard
* [Peachy Times](/chars/peachy_times) - daughter of mayor
* [Don Pianta](/chars/don_pianta) - leader of [Delfino Mafia](/orgs/delfino_mafia)
* [Underboss Fritters](/chars/underboss_fritters) - Underboss of Mafia.
* [Wacho Wench](/chars/wacho_wench) - owner of [Buxom Wench](delfino_plaza/shops/buxom_wench)