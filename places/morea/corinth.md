<!-- TITLE: Corinth -->
<!-- SUBTITLE: Grand city of Trade and Love -->

# Corinth
* Founded by [Helios](/gods/helios)

# Organizations
* A founding member of [Plato's Republic](/orgs/platos_republic)

# Notable Citizens
* [Diogenes the Philosopher](/chars/diogenes)
* [Edward the Philosopher](/chars/edward)