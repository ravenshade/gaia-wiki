<!-- TITLE: Daedalus Labyrinth -->
<!-- SUBTITLE: The greatest maze constructed -->

# Daedalus Labyrinth
## Map

### Labyrinth

<img src="/uploads/daedalus-labyrinth-crop.png" width="900" />

### Steamworks

<img src="/uploads/daedalus-labyrinth-underground-v-1.jpg" width="900" />

* Rule: "The only way to get through the maze is to think outside the maze."
* Inside a different interdimensional space. Time doesn't flow outside by using the people inside's years to reverse time.
* The labyrinth is split into five distinct sections. There is an underground clockwork running the labyrinth.
* From the exterior, large plants protect the labyrinth from intruders from above. The plant walls do as well.
* Overseen by Haskell
* See [notes](/notes/daedalus)

## Schedule
* 5 minutes - Maze hedges rotate
* 30 minutes - Sections rotate (garden, city, forest, mountains)
* 60 minutes - Grandfather clock dings (backwards)
* Timekeep steals time away to keep the time using the time magic mixed with blood magic. Every individual (aside from gnomish engineers) wears a watch that ticks. Watches are time-based-incrementing (second->minute) powerful if applied.

## Entrance
* The entrance prevents anyone from leaving. It closes in a "waiting room" until Haskell arrives.
* All entrants are given a free garden key, rule, and a portable clock.

# Notable Orgs
## Adventurers
Adventurers who got stuck on the first puzzle.
* Kori
* Peder
* Otis
## The Lost Ones
Children that have wandered into the maze and live there
Have figured out a lot of sewer tunnels around the maze, live in the forest
* Arissa
* Antony
* Castor
* Bishop
* Amara

## The Quarelling Statues
Statues that have been paralzed while throwing a tea party.
* Leader: Sally (the oldest)
* Chetty (the loudest)
* Lily (thinks Chetty cannot see)
* Ethan (complains the most)
* + Nop became a statue due to the favor.

## The Wardogs
A group of athenian soldiers "fed to the minotaur" but alive.
* Leader: [Vernon Alabaster](/chars/vernon-alabaster) - paladin - heavily injured by Basilisk. - gruff, old voice. Very direct and in charge.
* Avel Baptiste - supplier/handyman
* Old Man Brash - once solved the whole puzzle

## New Knossos
A small group of citizens who lived in Knossos. Formed a society of those interested in the maze. Decided to finally set out on an expedition. Ended up trapped.
* Leader: Lycaster Fitzgerald
* Odessa
* Kara
* Plutus

## Hole Diggers
Dwarves that have set out to dig their way out of the labyrinth. Constantly hunted by the Guardians.
* Leader: Krovil Broadrock
* Glavok Longfeet
* Brox Bladune

## Dolos Worshippers
Individuals who worship the God of Trickery statue near the basilisk.

## The Guardians
Massive constructs built by Daedalus to protect the maze. Will attack anyone in the Clockworks or harming the maze.

## The Gardeners
Small group of gnomes that take care of the maze. Run around. Hide in the hedges.

## The Sweepers
Massive mechanical constructs that clean up the Clockworks underneath.
# Places
## Gardens

## City

## Forest

## Mountains
* Grak the Troll - befriended

## Haunted Manor
* Maintenance workers died. Taken over by ghosts.
* See Beheaded, Diseased, Murdered
* Patriots killed the manor completely.

## The Impossible Maze
* Cannot be solved

## Maintenance Quarters

## The Clockworks


