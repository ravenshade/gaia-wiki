<!-- TITLE: Artem -->
<!-- SUBTITLE: The coastal city of Artem -->

# Artem
* Known for excellent archers, beast tamers, and woodsmen
* Produces the famous [Elven Bow](/items/elven_bow)
* Built the city in honor of [Artemis](/gods/artemis)

![Artem](/uploads/artem.jpg "Artem")
# Notable Residents
* [General Aegeus](/chars/aegeus) - Ruler (of the Despotate of Morea from Artem)
* [Theseus](/chars/theseus) - son of Aegeus, former King, wishes to kill minotaur & Minos @ Knossos
* Prince Jason - Looking for a band of heroes to help secure the Golden Fleece
* Peneus - father of Daphne, concerned about a man pursuing her
* [Daphne](/chars/daphne) - daughter of Peneus, being pursued by Apollo thanks to Eros.

## The Druids
* Rootgrowth, Jeremy (now Pianta) (daughter is Dandelion/Poppy), Poppin
* [Dandelion/Poppy](/chars/dandelion)

## Dryads
* Lily (hide&seek), Vine (leader)

## Eros' Clearing
* Jason - hit on Anne, she helped him with fighting
* Sephie - wants to poison Romulus with laxative gave to Emmett
* [Romulus](/chars/romulus) - douchebag wearing laurels with orc friend
* [Barry](/chars/barry_bitbucket) - engineering halfling, embarassed
* [Dandelion](/chars/dandelion) - halfling with white hair, daughter of Jeremy
* [Lucidius](/chars/lucidius) - elf in amore, loves dandelion, wants to be a resistance fighter

## Athenian Stronghold
* [Magister Tassos](/chars/magister-tassos) - Half-elf from Argos, runs Athenian Stronghold

## Temple of Artemis
* [Alleria](/chars/alleria) - Leader of Fletchers

# Organizations
* [The Fletcher Rebellion](/orgs/fletcher_rebellion)

# Locations
## Artem
 * City built within massive trees. Huge trunks and branches interconnect through bridges and other woodland constructions

## Temple of Artemis
* See [Temple of Artemis](/places/morea/temple_of_artemis)

## Hunting Woods
* Woods with numerous wildlife

## Archivale
* Village of druids living with dryads and nymphs

## Amore
* Small village where many men and women come to find love (mostly virgins)
* Surrounded by dryads & nymphs
* Often visit Eros' Clearing

## Eros' Clearing
* Famous site dedicated to the [Eros](/gods/eros)

## Marathon
* Famous for the running of the bulls
* Frequently receive bulls from all over Morea to race.

## Athenian Stronghold
* Massive barracks from Argos
* Ensure dominion of the territory of Artem

# Inhabitants
* Elves, but dissidants to the Despotate.
# Culture
* Very nature-bound, all about cultivating wildlife within cities.
* Native-American-like beliefs.
* Worships Alpheius and Artemis.

# Artem City
The city is divided into four major tiers. Each tier is a different vertical height on the massive tree of the city. The tree has two Pillars of Hercules intertwined within it.
## Map
![Artem](/uploads/artem.png "Artem City")

## Roots Tier
* Underground tunnels surrounding the large roots of the tree
* Weakspot in Artem, criminal culture
## Trunk Tier
* Stone pathways, homes & shops built inside the massive tree, staircases along smaller trees
* Shops, guilds, lower residential groups, shipyard

### Marina Quarter
* Docks
* Trueshot Lighthouse -- only lighthouse built into a tree
* General Marketplace
* The Stump -> The Spile - famous bar owned by Gump, convinced to rename, royalties agreed
* SharpTrees -- javelin shop

### Market Quarter
* Javelin Combat Academy
* Artem Maternity Hospital
* Quiver's Elixirs: Ring of Protection (family heirloom) taken (elf, angry)
* Carpenter's Guild
* Woodshafts - Woodcutter & Wood supplies store
* Tailor, Barber, Mason's

### Holy Quarter
* Artemis Temple
* Inns - Bark over Bite, The Grotto: bar & grill
* Stables
* Artemis' Grove - Monastery of female druids

### High Quarter
* Mercantile Residential Housing
* Lake Leto

### Backwoods
* Lower class housing
* Woodshaft's Mill
* [Vasso](/chars/vasso)'s Posions

### Core
* Bowyer's Guild
* Hunter's Guild
* Creature's Cove

## Canopy Tier
* Interconnected bridges, smaller trees, along heavy wood platforms, with large hole in tree
* Government, armory, Heart of the Tree, aristocratic housing, military housing
* Bramblebrush Arena
* large ampitheatre of government
* round building has paintings + carvings of Artemis' conquests
## Tops Tier
* Filled with massive birds and other creatures, the tops are considered dangerous territory.
* Wilderness smaller homes, vicious animals

# History
### The Delimore War
 * Artem was completely burnt to a crisp during the Delimore war during the siege on the city by the Delians and Spartans.
 * The Spartans were bloodthirsty, despite the behest of the Minotaur King of Knossos.
 * The Spartans burned the entire tree down.
