<!-- TITLE: Patras Manor -->
<!-- SUBTITLE: The Patriot's humble home -->

# Patras Manor
Pina
    Facing the sun
    Pianta Bed - leaves
    Bookshelf
Quad
    Mud, Planted Trees
    Sod
Lucius
    View of Ocean
    Bookshelves, study area, Orb Pedestal
Emmett
    Quiet Stoic Room
    Bed, Bookshelf, Comfortable Chair
Ariadne
    Bed, Study
Billy Bob & Croc room
Clockwork Workshop (Basement)
    Forge
    1 Unfinished Wall

Music Room
    Bagpipes (Straits of Gibraltar)
    
Guest Bedrooms
    Purple
    
Kitchen
    Find Cook - "Billy Bob"

Meeting Room
    15 individuals table
    
Meditation Room
    Ocean Facing
    
Training Room

Archery Range

Empty Library