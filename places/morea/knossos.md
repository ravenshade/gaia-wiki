<!-- TITLE: Knossos -->
<!-- SUBTITLE: The major port and naval city of Knossos -->

# Knossos
* Authoritarian Rulership

![Knossos](/uploads/knossos.jpg "Knossos")

* Heavy dwarven city. Southern dwarven accent.
# Map
<img src="/uploads/maps/knossos-map-v-1.jpg" width="900" />

# Notable Residents
* [Daedalus (missing)](/chars/daedalus) - great forgemaster and craftsmen of Knossos' forge
* [Barry Bitbucket (missing)](/chars/barry) - Suave entrepeneur 
* [Asterion - The Minotaur](/chars/asterion-minotaur) - Secret son of Minos, The Minotaur
* [Miles Flickerhouse (missing)](/chars/miles-flickerhouse) - Runs flickerhouse industries
* [General Rockfurt](/chars/general-rockfurt) - Delian Admiral, currently running Knossos
* Kreig the Blacksmith (friend w/ Emmett) & Master (Kroffmeister) the Blacksmith (dwarves)
* Swindley the Butler, Scribe, and Regal Postmaster (gnome)

## Notable Families
### Royal Family of Knossos
 * [King Minos (dead)](/chars/king-minos) - ruler of Knossos, formerly Crete (dead)
 * Queen Perisphae - (dead)
 * [Princess Ariadne](/gods/ariadne) - daughter of Minos, in love with Theseus
 * [Prince Androgeus (dead)](/chars/androgeus) - son of Minos
### Flickerhouse Family
Titans of new weapons industry.
### Henrietta Family
(Duke) weaponcrafters and boatmakers

 * Duke Adrian Henrietta - woodelf, weaponcrafter
 * Duchess Anna Henrietta (dead) - dwarf
### Thunderbrew Family
(Baron) rich brewers that are long established

 * [Thea Thunderbrew](/chars/thea-thunderbrew)
 * [Baroness Theadora Thunderbrew](/chars/theadora-thunderbrew)
 * [Baron Hammerlin Thunderbrew](/chars/hammerlin-thunderbrew)
### Barleybrew Family
(Count) rich brewers

 * Count Yorus Barleybrew - dwarf, brewer
 * Countess Maeve Barleybrew - dwarf, brewer
 * Sons: (all dead) Malgrin, Wellart, and Becan Barleybrew
### Marcheschi Family
(Count) rich vineyard owners

 * [Lydia Marcheschi](/chars/lydia-marcheschi)
 * [Countess Astrid Marcheschi](/chars/astrid-marcheschi)
# Locations (city)
Recently the city has been hunting down mages for the war effort.

## Castle

### Royal Quarters
 * Underground, deep underground. Next to the vault.
 * High ceilings and large bed. Large vault doors.

### Ariadne's Dwelling
 * Small unkempt 'apartment' that is rarely used or stocked.
 * Ariadne never spends much time in one place.

### Banquet Hall
 * Large tables, built to feed many. 

## Patras Manor
 * See [Patras Manor](/places/morea/knossos/patras-manor)
 * Near the Docks and Kreig's blacksmith
 * Formerly owned by Miles Flickerhouse.

## The Heady Topper
* Former brewery converted into a large furnace.
* Right next to the College of Glamour
* Nop is famous. 

## Cropton's
* A bar/pub turned into a home for a Dionysus Cult
* Well-known "Guard Krenal" goes there. Or did, until Emmett killed him.

## Twindley's
* A tailoring shop turned into a crab seafood restaurant
* Run by Twindley, a small halfling with excellent crab-sewing skills.

## Corona Brewery
* Shaped like a stout glass, full of colorful and fruity drinks.
* Run by Blorge Corona (orange pianta)

## Thunderbrew Brewery
 * Massive brewery, oldest in all districts.

## Knossos Lighthouse
* Massive lighthouse shaped like a beer bottle. Run by the local Corona Brewery.

## College of Glamour
* Famous for its young bard culture
* Also famous for its continued worship of Dionysus, debauchery, drinking.
* Restaurants nearby are often tailor-themed.
* Nop is a famous bard there.
# Locations

## Daedalus' Labyrinth
* [Daedalus' Labyrinth](/places/morea/daedalus-labyrinth)
* A labrynth built by Daedalus
* At the center is a powerful Minotaur
* Minotaur is regularly fed with athenian soldiers captured

## Taurus
* A city famous for its world-class bulls. Regularly races with Marathon.

## Wattle
* City of loggers that produce all the wood for Knossos' construction of ships
* Competitive with Daub.
* Previously a large vineyard
## Daub
* City of Stone, Clay, and Coal.
* Famous for acquiring massive amounts of ingredients for the recent war
* Competitive with Wattle
* Previously a brewery

## Mt. Pindus
* A lucrative and high mountain range in the lands of Morea.

## Cave of Artemis
* [Cave of Artemis](/places/morea/cave_of_artemis)

## Old Mine
* A location once regularly mined for coal, haunted by something now.

## Shrine of Alpheus
* Home of the minor god Alpheus
* Alpheius was in love with Artemis and attempted to rape all her nymphs

# Organizations
## Minoan Kingdom
* The great kingdom that rules over Knossos. Formerly a couple of brewers that started the city. Dwarves.

## Brashenfurt Company
* A massive industrial complex that produces swords, axes, boats, and iron. Previously a brewing company.
* Run by the Brashenfurt dwarven family

## Flickerhouse Industries
* A large engineering firm originally responsible for designing ships
* Originally owned by a dwarven family, now run by gnomes and halflings.

## Dionysus' Crawlers
* The main cult of dionysus that hides in taverns and spots around the city
* Recently cracked down on by the king
# Inhabitants
* Dwarves, Gnomes, Humans, occasionally orcs.
* Aristocracy mostly of gnomes and dwarves that build weapons and have taken over the town
# Economy
* The entire city has converted into weapon forging and smithing from nearby mines.
# Culture
* Strict, rigid, disciplinarian (recent)
* Previously famous for its luxury, wineries, breweries.
* Huge smoke clouds and smog now cloud the city and the surrounding area
* Previously worshipped [Dionysus](/gods/dionysus) but now worship [Hephaestus](/gods/hephaestus)

## Food & Wine
 * Greek food (Baklava, quail eggs, Dolmadakia), cheeses, grapes
 * Red-wine pears, Knossian shortbread
 * Wines/Brews famous throughout Gaia.
