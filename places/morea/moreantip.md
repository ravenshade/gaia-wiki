<!-- TITLE: The Morean Tip -->
<!-- SUBTITLE: The southern lush forests of Morea -->

# Map
![Morean Tip](/uploads/morean-tip.png "Morean Tip")
![Morean Tip Waterlogged 1](/uploads/maps/morean-tip-waterlogged-1.png "Morean Tip Waterlogged")

<img src="/uploads/maps/morean-tip-lower.jpg" width="800" />


# Organizations
* [Despotate of Morea](/orgs/despotate_of_morea)
* [Delian League](/orgs/delian_league)
# Locations
* [Babylonia](/places/morea/babylonia)
* [Artem](/places/morea/artem)
* [Knossos](/places/morea/knossos)
* Isle of [Argos](/places/morea/argos)
* Isle of [Delos](/places/morea/delos)
* Isle of Alexandria
# Inhabitants
* [Nymphs](/races/nymph) are found frequently near the rivers and lakes.
* [Dryads](/races/dryad) are found scattered amongst the forests surrounding Artem.
* Elves inhabit the main cities and forests around.
* Half-Elves mostly inhabit [Knossos](/places/morea/knossos)
* Dwarves inhabit the mountains near the center of the tip.
# Economy
The economy was originaly centered around agriculture and logging. The delimore war has brought great change to the region. 
The once great fields and forests have been savagely burned in war-torn areas around.
# Culture
Conflict between radically different cultures causes substantial conflict. 
* Democracy is purported by the southern isle of Argos which has now spread resistantly to Artem.
* Monarchy is encourage by the state of Knossos and Babylon. 
* Babylon's sudden disappearance has caused an abundance of resources and deserters from both forces has led to strong bandit camps

# History
## Delimore War

Political map

<img src="/uploads/maps/morean-tip-lower-forces.png"  width="900" />