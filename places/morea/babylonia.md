<!-- TITLE: Babylonia -->
<!-- SUBTITLE: The Great Fields of Babylon -->

# Babylonia
* Located within [the Morean Tip](/places/morea/morean_tip).
* Originally home to the legendary city of [Babylon](/places/babylon) before the land began sinking
* Contains a [Pillar of Hercules](/places/pillars_of_hercules) under the [Tower of Babel](/places/morea/tower_of_babel)
* Massive fields of wheat make up the majority of the land
* The site of many battles in the [Delimore War](/events/delimore_war)
# Notable Residents
* TODO

# Locations
## Hanging Gardens
* See [Ruins of the Hanging Gardens](/places/morea/hanginggardens)
## Tower of Babel
* See [Tower of Babel](/places/morea/tower_of_babel)

## Ur
A town of bandits and thieves compromise the city

## Babylon's Harvest
Previously the famous wheat fields of Babylon, now sunk partially into the ground.
The site of the Last Battle of Babylon where a [Reaper](/races/reaper) is harvesting.

## Crater of Babylon
The leftovers of the city after it was launched into the sky

## Walls of Babylon
Famous walls that surround all of Babylonia and Babylon itself. The walls are in disrepair after the escape of the city.
# Inhabitants
* Bandits, looters, thieves scatter the ruins of the land
# Culture
* Originally a culture defined in craftsmanship, engineering, and the study of magic.
* Transitioned to a war-torn selfish culture
* Poseidon worship has skyrocketed to stop the sea from consuming land