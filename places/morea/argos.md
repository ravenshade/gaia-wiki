<!-- TITLE: Argos -->
<!-- SUBTITLE: The mystical and delirius city of Argos -->

# Argos
* Home of Democracy
* Residents known as Argives
* Founded by "Three Kings of Argos" (all mages of different types)
* Competitive with [Arcadia](/places/morea/arcadia), another mage city

![Argos](/uploads/argos.jpg "Argos")
# Map
TODO
# Notable Residents
* Alexander the Great - original home when not on the Isle of Alexandria
* [Sophocles](/chars/sophocles) - great writer

# Organizations
* The Argonauts -- a band of heroes who seek the Golden Fleece
* The Assembly -- major form of government
* The Council -- magistrates, decision leaders in country
* [Despotate of Morea](/orgs/despotate_of_morea) -- founding member
# Locations
# Inhabitants
# Economy
* Magic-based economy on food
* Known for citrus fruits & melon
# Culture
* City of mages and magic that originally focused purely on food production
* Founded democracy becauase all mages competing was too dangerous