<!-- TITLE: Athens -->
<!-- SUBTITLE: A large cityscape near a gulf of Morea. -->

# The City of Athens
* Founded by the goddess [Athena](/gods/athena)

![4 Iccimz 1 Pvwf 83 Sxhajcuom 8 Kqmoqf 6 V 7 Wv 9 Jdrlszg](/uploads/4-iccimz-1-pvwf-83-sxhajcuom-8-kqmoqf-6-v-7-wv-9-jdrlszg.jpg "4 Iccimz 1 Pvwf 83 Sxhajcuom 8 Kqmoqf 6 V 7 Wv 9 Jdrlszg")
# Map
TODO

# Organizations
* Capital city of the [Despotate of Morea](/orgs/despotate_of_morea)
* Led by [Plato's Republic](/orgs/platos_republic)
* Hosts the meeting of the [Seven Sages of Gaia](/orgs/seven_sages_of_gaia)
* Hosts the major [Stock Market](/orgs/stock_market)

# Notable Residents
* [Socrates](/chars/socrates)
* [Plato](/chars/plato)
* [Aristotle](/chars/aristotle)
* [Archimedes](/chars/archimedes)
* [Pythagoras](/chars/pythagoras)

# Locations

