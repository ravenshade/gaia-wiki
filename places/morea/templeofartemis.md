<!-- TITLE: Temple of Artemis -->
<!-- SUBTITLE: A massive temple dedicated to Artemis -->

# Temple of Artemis
* Contains a [Pillar of Hercules](/places/pillarofhercules)
* Entrance requires the Honor of Artemis

# The Honor of Artemis
Each person in order to get the honor of artemis must meet a certain set of her requirements
* Help a Nymph or Dryad
* Help a Virgin / save a virgin
* Do a feat of great archery
* conquer a mountain

# Temple
## Artemis' Challenge
* hunt down the legendary chimera

## Fletcher Rebellion HQ
* Orlov the Rebel