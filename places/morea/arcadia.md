<!-- TITLE: Arcadia -->
<!-- SUBTITLE: The magic-based city of Arcadia -->

# Arcadia
* Run by a Mageocracy and Druidic Circle
* Surrounded by a giant forest and magical fey woodland
* The lands are effectively fey in the nearby region. 
# Map
TODO
# Notable Residents
* [The God Pan](/gods/pan)
* [The Bandu Troupe](/chars/bandu-troupe)

# Organizations
# Locations
 * [The Cave of Ten Thousand Monsters](arcadia/cave-of-ten-thousand-monsters)
 * [Athena's Library](arcadia/athenas-library)
# Inhabitants
# Economy
# Culture