<!-- TITLE: Athenas Library -->
<!-- SUBTITLE: The Legendary Library of All Knowledge -->

# Athena's Library
 * Actually owned by the spirit Minerva, a disciple of Athena.
 * Minerva is a massive barn owl with huge black wings.
 * The library is protected by small white foxes nymphs that gather knowledge from the world.

## The Blessing of Hera
When the Pillars of Hercules were built, they were blessed by Hera to hold up the Earth. 
This blessing acted like a spell, protecting the pillars from any sort of destruction.

However, the spell can be dismissed by a powerful ritual. [Duplicamos](/chars/duplicamos) (master) broke the spell.