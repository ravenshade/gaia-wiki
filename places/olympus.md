<!-- TITLE: Olympus -->
<!-- SUBTITLE: The home of the Greek Gods -->

# Olympus
* Reached through the [mountain of Olympia](/places/delia/olympia)

# Locations
* [Hercules' Colosseum](/places/olympus/hercules_colosseum) - The Grand stage of fighting for the gods