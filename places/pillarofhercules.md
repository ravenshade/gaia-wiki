<!-- TITLE: Pillar of Hercules -->
<!-- SUBTITLE: Pillar of Hercules - the pillars hanging up the world -->

# Info
* Around 400-500 feet tall usually
# Locations
* [Turre Antiqui](/places/gelatin/turre_antiqui) - destroyed
* [Cecily](/places/cecily) - under construction
* [Temple of Artemis](/places/morea/templeofartemis)
* [Tower of Babel](/places/morea/tower_of_babel) - Massively chipped
* [Artem Tree](/places/morea/artem) -- Two inside the tree

# Plans
* [Construction of Pillars](/plans/pillarofherculesconstruction)
* [Destruction of Pillars](/plans/pillarsofherculesdestruction)