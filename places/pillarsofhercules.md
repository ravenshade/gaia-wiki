<!-- TITLE: Pillars of Hercules -->
<!-- SUBTITLE: The Pillars that Hold the World -->

# Pillars
1.  Center of the world under [Turre Antiqui](/places/turre_antiqui) [(destroyed)](/plans/pillars_of_hercules_destruction)
2.  Under the [Tower of Babel](/places/morea/tower_of_babel) (destroyed)
3.  Under Temple of Artemis
4.  (2) Inside the Great Tree of Artem
2.  Under [Temple of Apollo](/places/delia/temple_of_apollo)
4.  Under [Collosus of Rhodes](/places/delia/collosus_of_rhodes)
5.  Under [Olympia](/places/delia/olympia)
6.  Under the city of [Thebes](/places/morea/thebes)
7.  Two located under [Divendi](/places/morea/divendi)
8.  ??? The Forgotten Pillar